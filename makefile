# makefile to compile my1todotxt

TDSRCS = my1cstr.o my1cstr_util.o my1cstr_line.o my1strtok.o
TDSRCS += my1text.o my1list.o my1list_sort.o my1date.o todo.o
TARGET = my1todo-gui
SOURCE = window_main.h $(TARGET).o gtkapp.o data.o my1path.o $(TDSRCS)
SIMPLE = my1todo
OBJLST = data.o $(SIMPLE).o my1path.o $(TDSRCS)

DOSRCS = $(subst src/,,$(sort $(wildcard src/todo*.c)))
DOTGTS = $(subst .c,,$(DOSRCS))

CC = gcc

CFLAGS = -Wall
ifeq ($(DO_STATIC),yes)
CFLAGS += -static
endif
# nice to have this... my1codelib project
EXTPATH = ../my1codelib/src
CFLAGS += -I$(EXTPATH)/

# SIMPLE - be static!
$(SIMPLE): CFLAGS += -static

# to include buildh output
$(TARGET): CFLAGS += -I./
# TARGET needs gtk
$(TARGET): CFLAGS += $(shell pkg-config --cflags gtk+-3.0)
$(TARGET): LDLIBS += $(shell pkg-config --libs gtk+-3.0) -export-dynamic

$(SIMPLE): $(OBJLST)
	$(CC) $(CFLAGS) -o $@ $^ $(LDLIBS)
	strip $(SIMPLE)

$(TARGET): $(SOURCE)
	$(CC) $(CFLAGS) -o $@ $(SOURCE) $(LDLIBS)

.PHONY: all con gui new clean

all: con gui

con: $(SIMPLE)

gui: $(TARGET)

new: clean gui

$(DOTGTS): CFLAGS += -Wall -D_TEST_$(shell echo $@|tr a-z A-Z)_
$(DOTGTS): % : src/%.c src/%.h
	gcc $(CFLAGS) -o $@ $< $(LFLAGS) $(OFLAGS)

%.o: src/%.c src/%.h
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: src/%.c
	$(CC) -c $(CFLAGS) -o $@ $<

%.h: glade/%.glade
	bash buildh $@

# my1codelib include
%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) -c $(CFLAGS) -o $@ $<

clean:
	rm -rf $(TARGET) $(SIMPLE) $(DOTGTS) *.o *.h
