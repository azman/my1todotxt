/*----------------------------------------------------------------------------*/
#ifndef __MY1GTKAPP_H__
#define __MY1GTKAPP_H__
/*----------------------------------------------------------------------------*/
#include <gtk/gtk.h>
/*----------------------------------------------------------------------------*/
#include "data.h"
/*----------------------------------------------------------------------------*/
#define MY1FONT_MONO_INIT "<span font_desc=\"mono 12\">"
#define MY1FONT_MONO_ENDS "</span>"
#define MY1FONT_BOLD_INIT "<b>"
#define MY1FONT_BOLD_ENDS "</b>"
#define MY1FONT_DONE_INIT "<s>"
#define MY1FONT_DONE_ENDS "</s>"
/*----------------------------------------------------------------------------*/
#define MY1DATA(gapp) ((data_t*)gapp)
#define MY1TASK_KEY "my1"
/*----------------------------------------------------------------------------*/
#define QASK_NO 0
#define QASK_YES 1
#define QASK_CANCEL 2
/*----------------------------------------------------------------------------*/
#define FLAG_DO_FULL (FLAG_LFLAG<<1)
#define FLAG_CHKUUID (FLAG_LFLAG<<2)
/*----------------------------------------------------------------------------*/
typedef struct _my1gtkapp_t {
	data_t data;
	my1cstr_t nbuf, tbuf, ibuf;
	GtkWidget *main, *hbar, *swin, *bfil;
	GtkWidget *view, *vbox, *cncb, *pjcb;
	GtkWidget *open, *save, *sort, *tfil;
	GtkWidget *texe, *bexe, *rexe, *menu;
	GtkWidget *wtmp; /* temp widget used in gtkapp_pass_show */
	GtkWidget *pick; /* picked task */
	char *preq, *psrc;
	my1todo_task_t* task; /* pure pointer, general purpose */
	my1todo_name_t* name; /* pure pointer to items in cons/pros */
	int loop, temp; /* temporary storage */
} my1gtkapp_t;
/*----------------------------------------------------------------------------*/
void gtkapp_init_data(my1gtkapp_t* gapp, int argc, char* argv[]);
void gtkapp_free_data(my1gtkapp_t* gapp);
void gtkapp_flag_exec(my1gtkapp_t* gapp, char* flag, int what);
void gtkapp_pass_show(my1gtkapp_t* gapp);
void gtkapp_data_update(my1gtkapp_t* gapp);
void gtkapp_load_data(my1gtkapp_t* gapp, char* name);
void gtkapp_save_data(my1gtkapp_t* gapp, char* name);
void gtkapp_done_data(my1gtkapp_t* gapp);
void gtkapp_todo_modified(my1gtkapp_t* gapp, int yes);
void gtkapp_todo_file_sort(my1gtkapp_t* gapp);
void gtkapp_todo_file_open(my1gtkapp_t* gapp);
void gtkapp_todo_file_save(my1gtkapp_t* gapp);
/*----------------------------------------------------------------------------*/
#endif /* __MY1GTKAPP_H__ */
/*----------------------------------------------------------------------------*/
