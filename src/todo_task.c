/*----------------------------------------------------------------------------*/
#include "todo_task.h"
/*----------------------------------------------------------------------------*/
#ifndef __MY1TODO_TASK_C__
#define __MY1TODO_TASK_C__
/*----------------------------------------------------------------------------*/
#include "my1cstr_util.h"
#include "my1strtok.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void tdtask_free_tags(void* that) {
	my1item_t *item = (my1item_t*)that;
	tdtags_free((my1todo_tags_t*)item->data);
	free(that);
}
/*----------------------------------------------------------------------------*/
void tdtask_init(my1todo_task_t* task, my1todo_type_t* type) {
	tddata_init((my1todo_data_t*)task);
	task->made.date = DATE_INVALID;
	task->made.time = TIME_INVALID;
	task->done.date = DATE_INVALID;
	task->ddue.date = DATE_INVALID;
	cstr_init(&task->text);
	cstr_init(&task->prep);
	cstr_init(&task->prio);
	cstr_init(&task->task);
	list_init(&task->tags,tdtask_free_tags);
	/* pure pointers in cons/pros - data in main list */
	list_init(&task->cons,list_free_item);
	list_init(&task->pros,list_free_item);
	task->type = type;
	task->vobj = 0x0;
}
/*----------------------------------------------------------------------------*/
void tdtask_free(my1todo_task_t* task) {
	list_free(&task->tags);
	list_free(&task->pros);
	list_free(&task->cons);
	cstr_free(&task->task);
	cstr_free(&task->prio);
	cstr_free(&task->prep);
	cstr_free(&task->text);
}
/*----------------------------------------------------------------------------*/
int tdtask_scan_task(my1todo_task_t* task, char* word) {
	my1todo_name_t *find;
	my1todo_tags_t *tags, test;
	my1time_t temp;
	int skip = 0;
	if (task->type) {
		/* only if list is available */
		if (word[0]=='@'&&word[1]!=0x0) {
			find = tdtype_make_cons(task->type,word);
			if (find) list_push_item_data(&task->cons,find);
			skip = 1;
		}
		else if (word[0]=='+'&&word[1]!=0x0) {
			find = tdtype_make_pros(task->type,word);
			if (find) list_push_item_data(&task->pros,find);
			skip = 1;
		}
	}
	if (!skip) {
		/* look for tags if NOT context NOR project */
		tdtags_init(&test);
		if (tdtags_make(&test,word)>=0) {
			/* valid tag! */
			tags = tdtask_make_tags(task,&test);
			if (tags) {
				/* process useful tags */
				if (!strncmp(test.akey.buff,"uid",4)) {
					time_my1str(&temp,test.aval.buff);
					if (temp.time!=TIME_INVALID)
						task->made.time = temp.time;
				}
				else if (!strncmp(test.akey.buff,"due",4)) {
					date_isostr(&task->ddue,test.aval.buff);
					/* ignore if invalid - not enforced! */
				}
			}
		}
		tdtags_free(&test);
	}
	/* append to task */
	cstr_addcol(&task->task,word,MY1CSTR_PREFIX_SPACING);
	return 0;
}
/*----------------------------------------------------------------------------*/
int tdtask_read(my1todo_task_t* task, char* line) {
	char* look;
	my1strtok_t find;
	int iret = 0;
	/* save local copy */
	cstr_assign(&task->text,line);
	strtok_init(&find," ");
	do {
		strtok_prep(&find,&task->text);
		if (!strtok_next(&find)) {
			iret = TDTASK_ERROR_EMPTY_LINE;
			break;
		}
		/* clear completion flag, priority */
		tddata_flag_clr((my1todo_data_t*)task,TDTASK_FLAG_COMPLETED);
		cstr_null(&task->prio);
		/* check for priority/complete marks */
		look = find.token.buff;
		if (look[0]=='x'&&find.token.fill==1) {
			tddata_flag_set((my1todo_data_t*)task,TDTASK_FLAG_COMPLETED);
			if (!strtok_next(&find)) {
				iret = TDTASK_ERROR_INVALID1_LINE;
				break;
			}
			look = find.token.buff;
		}
		if (look[0]=='(') {
			if (find.token.fill!=3||look[2]!=')'||look[1]<0x41||look[1]>0x5b) {
				iret = TDTASK_ERROR_INVALID2_LINE;
				break;
			}
			cstr_append(&task->prio,look);
			if (!strtok_next(&find)) {
				iret = TDTASK_ERROR_INVALID3_LINE;
				break;
			}
			look = find.token.buff;
		}
		/* clear dates */
		task->made.date = DATE_INVALID;
		task->made.time = TIME_INVALID;
		task->done.date = DATE_INVALID;
		task->ddue.date = DATE_INVALID;
		/* check for dates: completion and creation */
		if (look[0]>=0x30&&look[0]<=0x39) {
			/* assume completion date */
			date_isostr(&task->done,look);
			if (task->done.date==DATE_INVALID) {
				iret = TDTASK_ERROR_INVALID4_LINE;
				break;
			}
			if (!strtok_next(&find)) {
				iret = TDTASK_ERROR_INVALID5_LINE;
				break;
			}
			look = find.token.buff;
			/* check if that was actually creation date */
			if (look[0]<0x30||look[0]>0x39) {
				task->made.date = task->done.date;
				task->done.date = DATE_INVALID;
			}
			else {
				date_isostr((my1date_t*)&task->made,look);
				if (task->made.date==DATE_INVALID) {
					iret = TDTASK_ERROR_INVALID6_LINE;
					break;
				}
				if (!strtok_next(&find)) {
					iret = TDTASK_ERROR_INVALID7_LINE;
					break;
				}
				look = find.token.buff;
			}
		}
		/* the rest is task - put in residue first */
		cstr_null(&task->task);
		/* reset everything! */
		list_redo(&task->tags);
		list_redo(&task->pros);
		list_redo(&task->cons);
		/* scan the words for contexts / projects / tags */
		while (look||strtok_next(&find)) {
			if (!look) look = find.token.buff;
			tdtask_scan_task(task,look);
			look = 0x0;
		}
	} while (0);
	strtok_free(&find);
	return iret;
}
/*----------------------------------------------------------------------------*/
int tdtask_prep(my1todo_task_t* task) {
	/* prep prefix */
	tdtask_prep_str(task,&task->prep);
	/* dump task */
	cstr_append(&task->prep,task->task.buff);
	return 0;
}
/*----------------------------------------------------------------------------*/
int tdtask_prep_str(my1todo_task_t* task, my1cstr_t* pfix) {
	cstr_null(pfix);
	/* is this completed */
	if (tdtask_completed(task))
		cstr_append(pfix,"x ");
	/* check priority */
	if (task->prio.fill>0)
		cstr_addcol(pfix,task->prio.buff,MY1CSTR_SEP_SPACING);
	/* check completion date */
	if (task->done.date!=DATE_INVALID)
		cstr_addcol(pfix,date_2str(&task->done,0x0),MY1CSTR_SEP_SPACING);
	/* check creation date */
	if (task->made.date!=DATE_INVALID)
		cstr_addcol(pfix,
			date_2str((my1date_t*)&task->made,0x0),MY1CSTR_SEP_SPACING);
	return 0;
}
/*----------------------------------------------------------------------------*/
int tdtask_view_str(my1todo_task_t* task, my1cstr_t* buff, int mask) {
	cstr_null(buff);
	if (mask&TDTASK_VIEW_UUID) {
		cstr_append(buff,"[");
		cstr_append(buff,time_2my1_full((my1time_t*)&task->uuid,0x0));
		cstr_append(buff,"] ");
	}
	if (mask&TDTASK_VIEW_FULL)
		cstr_append(buff,task->text.buff);
	else {
		/* default view */
		if (task->prio.fill>0)
			cstr_addcol(buff,task->prio.buff,MY1CSTR_SEP_SPACING);
		else
			cstr_append(buff,"    ");
		/* filter out uid: tag from task */
		cstr_append(buff,task->task.buff);
		cstr_rmword(buff,"uid:"," ");
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int tdtask_calc_due(my1todo_task_t* task, my1date_t* curr) {
	my1time_t temp;
	if (task->ddue.date==DATE_INVALID) return TDTASK_DUE_NONE;
	if (!curr) {
		time_read(&temp,1); /* get today */
		curr = (my1date_t*)&temp;
	}
	/* date_subtract handles curr > ddue! */
	temp.time = date_subtract(&task->ddue,curr);
	if (temp.time>=0) temp.time++; /* today's date (@0) count as 1! */
	return temp.time; /* should not be zero here? */
}
/*----------------------------------------------------------------------------*/
int tdtask_form_my1(my1todo_task_t* task) {
	my1todo_tags_t temp;
	char buff[32];
	int stat;
	stat = 0;
	/* add creation date if NOT given */
	if (task->made.date==DATE_INVALID) {
		task->made.date = task->uuid.date;
		/* most probably invalid too (no uid: tag) */
		task->made.time = task->uuid.time;
		stat++;
	}
	else if (task->made.time==TIME_INVALID) {
		task->made.time = 0;
		stat++;
	}
	/* add uid: tag if not found */
	if (!tdtask_find_tags(task,"uid")) {
		strcpy(buff,"uid:");
		date_time_pick_format(DATETIME_PICK_MY1FORMAT);
		strcat(buff,time_2str(&task->made,0x0));
		date_time_pick_format(DATETIME_PICK_DEFAULT);
		tdtags_init(&temp);
		if (tdtags_make(&temp,buff)>=0) {
			if (tdtask_make_tags(task,&temp)) {
				cstr_addcol(&task->task,buff,MY1CSTR_PREFIX_SPACING);
				stat++;
			}
		}
		tdtags_free(&temp);
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int tdtask_done(my1todo_task_t* task) {
	my1todo_tags_t* tags, ctag;
	my1time_t temp;
	int test, size;
	/* set completion date */
	time_read(&temp,1);
	task->done.date = temp.date;
	test = 0;
	if (task->ddue.date!=DATE_INVALID) {
		task->ddue.date = DATE_INVALID;
		test++;
	}
	/* remove priority - if any */
	if (task->prio.fill>0) {
		char buff[8] = "pri:x";
		buff[4] = task->prio.buff[1];
		cstr_null(&task->prio);
		/* convert to pri tags */
		tdtags_init(&ctag);
		if (tdtags_make(&ctag,buff)>=0) {
			tags = tdtask_make_tags(task,&ctag);
			if (!tags)
				test |= TDTASK_ERROR_REMOVING_DDUE;
		}
		tdtags_free(&ctag);
		/* append to task */
		cstr_addcol(&task->task,buff,MY1CSTR_PREFIX_SPACING);
	}
	/* remove due tags - if found */
	tags = tdtask_find_tags(task,"due");
	if (tags) {
		if (!list_hack_item(&task->tags,(void*)tags))
			test |= TDTASK_ERROR_REMOVING_DDUE;
		/* remove tags manually */
		tdtask_free_tags((void*)tags);
		/** also remove from task string */
		size = cstr_rmword(&task->task,"due:"," ");
		if (!size)
			test |= TDTASK_ERROR_REMOVING_DDUE;
	}
	tdtask_complete(task);
	tdtask_prep(task);
	cstr_assign(&task->text,task->prep.buff);
	return test;
}
/*----------------------------------------------------------------------------*/
my1todo_tags_t* tdtask_find_tags(my1todo_task_t* task, char* akey) {
	my1todo_tags_t *find;
	list_scan_prep(&task->tags);
	while (list_scan_item(&task->tags)) {
		find = (my1todo_tags_t*) task->tags.curr->data;
		if (!strncmp(akey,find->akey.buff,find->akey.fill+1))
			return find;
	}
	return 0x0;
}
/*----------------------------------------------------------------------------*/
my1todo_tags_t* tdtask_make_tags(my1todo_task_t* task, my1todo_tags_t* from) {
	my1todo_tags_t* tags = (my1todo_tags_t*) malloc(sizeof(my1todo_tags_t));
	if (tags) {
		tdtags_init(tags);
		if (from) tdtags_copy(tags,from);
		list_push_item_data(&task->tags,tags);
	}
	return tags;
}
/*----------------------------------------------------------------------------*/
#ifdef _TEST_TODO_TASK_
/*----------------------------------------------------------------------------*/
#include "my1date.c"
#include "my1cstr.c"
#include "my1cstr_line.c"
#include "my1cstr_util.c"
#include "my1strtok.c"
#include "my1list.c"
#include "todo_data.c"
#include <stdio.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1cstr_t buff, view;
	my1todo_type_t type;
	my1todo_task_t task;
	my1todo_name_t *ptmp;
	FILE* fptr;
	int stat, test, step;
	cstr_init(&buff);
	cstr_init(&view);
	tdtype_init(&type);
	tdtask_init(&task,&type);
	do {
		stat = 0; step = 0;
		if (argc<2) {
			printf("** No input file given!\n");
			stat = -1;
			break;
		}
		fptr = fopen(argv[1],"rt");
		if (!fptr) {
			printf("** Cannot open file '%s'!\n",argv[1]);
			stat = -1;
			break;
		}
		printf("@@ Reading '%s'...\n",argv[1]);
		while ((test=cstr_read_line(&buff,fptr))>=0) {
			step++;
			if (buff.fill) {
				test = tdtask_read(&task,buff.buff);
				if (!test) {
					tdtask_view_str(&task,&view,TDTASK_VIEW_DEFAULT);
					printf("## %s %s\n",tdtask_completed(&task)?
						"[DONE]":"[TASK]",view.buff);
					if (task.cons.size) {
						printf("   > Context:");
						list_scan_prep(&task.cons);
						while (list_scan_item(&task.cons)) {
							ptmp = (my1todo_name_t*)task.cons.curr->data;
							printf(" %s",ptmp->name.buff);
						}
						printf("\n");
					}
					if (task.pros.size) {
						printf("   > Project:");
						list_scan_prep(&task.pros);
						while (list_scan_item(&task.pros)) {
							ptmp = (my1todo_name_t*)task.pros.curr->data;
							printf(" %s",ptmp->name.buff);
						}
						printf("\n");
					}
					cstr_null(&view);
					if (task.prio.fill) {
						cstr_append(&view," Priority:");
						cstr_append(&view,task.prio.buff);
					}
					if (task.done.date!=DATE_INVALID) {
						cstr_append(&view," [Done@");
						cstr_append(&view,
							date_2my1((my1date_t*)&task.done,0x0));
						cstr_append(&view,"]");
					}
					if (task.made.date!=DATE_INVALID) {
						cstr_append(&view," [Made@");
						cstr_append(&view,
							date_2my1((my1date_t*)&task.made,0x0));
						cstr_append(&view,"]");
					}
					if (task.ddue.date!=DATE_INVALID) {
						cstr_append(&view," [Due@");
						cstr_append(&view,
							date_2my1((my1date_t*)&task.ddue,0x0));
						cstr_append(&view,"]");
					}
					if (view.fill) printf("   >%s\n",view.buff);
				}
				else {
					printf("** Invalid my1TODOtxt format!\n");
					printf("   > [Line:%3d] '%s' (%d:%d/%d)\n",step,
							buff.buff,test,buff.fill,buff.size);
				}
			}
		}
		fclose(fptr);
		printf("@@ Done reading '%s'.\n",argv[1]);
	} while(0);
	printf("## All Context(s):");
	list_scan_prep(&type.cons);
	while (list_scan_item(&type.cons)) {
		ptmp = (my1todo_name_t*)type.cons.curr->data;
		printf(" %s",ptmp->name.buff);
	}
	printf("\n");
	printf("## All Project(s):");
	list_scan_prep(&type.pros);
	while (list_scan_item(&type.pros)) {
		ptmp = (my1todo_name_t*)type.pros.curr->data;
		printf(" %s",ptmp->name.buff);
	}
	printf("\n");
	tdtask_free(&task);
	tdtype_free(&type);
	cstr_free(&view);
	cstr_free(&buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif /* _TEST_TODO_TASK_ */
/*----------------------------------------------------------------------------*/
#endif /** __MY1TODO_TASK_C__ */
/*----------------------------------------------------------------------------*/
