/*----------------------------------------------------------------------------*/
#ifndef __MY1TODO_DATA_H__
#define __MY1TODO_DATA_H__
/*----------------------------------------------------------------------------*/
#include "my1date.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1todo_data_t {
	my1date_time_t uuid; /* use local timestamp for id! */
} my1todo_data_t;
/*----------------------------------------------------------------------------*/
#define TDDATA(pd) ((my1todo_data_t*)pd)
#define tddata_flag_set(pd,fl) TDDATA(pd)->uuid.tpad|=fl
#define tddata_flag_clr(pd,fl) TDDATA(pd)->uuid.tpad&=~fl
#define tddata_flag_get(pd,fl) (TDDATA(pd)->uuid.tpad&fl)
#define tddata_flag_chk(pd,fl) ((TDDATA(pd)->uuid.tpad&fl)==fl)
/*----------------------------------------------------------------------------*/
void tddata_init(my1todo_data_t* data);
void tddata_make(my1todo_data_t* data, int date, int time);
char* tddata_uuid(my1todo_data_t* data);
/*----------------------------------------------------------------------------*/
#include "my1cstr.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1todo_name_t {
	my1date_time_t uuid;
	my1cstr_t name;
} my1todo_name_t;
/*----------------------------------------------------------------------------*/
void tdname_init(my1todo_name_t* name);
void tdname_free(my1todo_name_t* name);
void tdname_make(my1todo_name_t* name, char *pstr);
/*----------------------------------------------------------------------------*/
typedef struct _my1todo_tags_t {
	my1date_time_t uuid;
	my1cstr_t akey, aval;
} my1todo_tags_t;
/*----------------------------------------------------------------------------*/
void tdtags_init(my1todo_tags_t* tags);
void tdtags_free(my1todo_tags_t* tags);
int tdtags_make(my1todo_tags_t* tags, char* pstr);
int tdtags_copy(my1todo_tags_t* tags, my1todo_tags_t* from);
/*----------------------------------------------------------------------------*/
#include "my1list.h"
/*----------------------------------------------------------------------------*/
/* main db for contexts and projects */
typedef struct _my1todo_type_t {
	my1date_time_t uuid;
	my1list_t cons; /* list of my1todo_name_t (contexts) */
	my1list_t pros; /* list of my1todo_name_t (projects) */
} my1todo_type_t;
/*----------------------------------------------------------------------------*/
void tdtype_init(my1todo_type_t* type);
void tdtype_free(my1todo_type_t* type);
/*----------------------------------------------------------------------------*/
#define TDTYPE(pt) ((my1todo_type_t*)pt)
#define tdtype_find_cons(pt,pn) tdtype_find(&TDTYPE(pt)->cons,pn)
#define tdtype_make_cons(pt,pn) tdtype_make(&TDTYPE(pt)->cons,pn)
#define tdtype_find_pros(pt,pn) tdtype_find(&TDTYPE(pt)->pros,pn)
#define tdtype_make_pros(pt,pn) tdtype_make(&TDTYPE(pt)->pros,pn)
/*----------------------------------------------------------------------------*/
my1todo_name_t* tdtype_find_uuid(my1list_t* list, my1todo_name_t* curr);
my1todo_name_t* tdtype_find(my1list_t* list, char* name);
my1todo_name_t* tdtype_make(my1list_t* list, char* name);
/*----------------------------------------------------------------------------*/
#endif /** __MY1TODO_DATA_H__ */
/*----------------------------------------------------------------------------*/
