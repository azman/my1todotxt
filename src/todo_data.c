/*----------------------------------------------------------------------------*/
#include "todo_data.h"
/*----------------------------------------------------------------------------*/
#ifndef __MY1TODO_DATA_C__
#define __MY1TODO_DATA_C__
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void tddata_init(my1todo_data_t* data) {
	date_time_diff(&data->uuid,1); /* using localtime */
	data->uuid.tpad = 0; /* reset flag! */
}
/*----------------------------------------------------------------------------*/
void tddata_make(my1todo_data_t* data, int date, int time) {
	/* should be valid date/time */
	data->uuid.date = date;
	data->uuid.time = time;
}
/*----------------------------------------------------------------------------*/
char* tddata_uuid(my1todo_data_t* data) {
	return date_time_zutc(&data->uuid,0x0);
}
/*----------------------------------------------------------------------------*/
void tdname_init(my1todo_name_t* name) {
	tddata_init((my1todo_data_t*)name);
	cstr_init(&name->name);
}
/*----------------------------------------------------------------------------*/
void tdname_free(my1todo_name_t* name) {
	cstr_free(&name->name);
}
/*----------------------------------------------------------------------------*/
void tdname_make(my1todo_name_t* name, char *pstr) {
	cstr_assign(&name->name,pstr);
}
/*----------------------------------------------------------------------------*/
void tdtags_init(my1todo_tags_t* tags) {
	tddata_init((my1todo_data_t*)tags);
	cstr_init(&tags->akey);
	cstr_init(&tags->aval);
}
/*----------------------------------------------------------------------------*/
void tdtags_free(my1todo_tags_t* tags) {
	cstr_free(&tags->aval);
	cstr_free(&tags->akey);
}
/*----------------------------------------------------------------------------*/
int tdtags_make(my1todo_tags_t* tags, char* pstr) {
	int chk1 = 0, chk2 = -1;
	while (pstr[chk1]) {
		if (pstr[chk1]==':') {
			if (chk2<0) chk2 = chk1;
			else return -1; /* multiple ':' */
		}
		chk1++;
	}
	if (chk2<0) return -1; /* cannot find ':' */
	pstr[chk2] = 0x0;
	cstr_assign(&tags->akey,pstr);
	pstr[chk2++] = ':';
	cstr_assign(&tags->aval,&pstr[chk2]);
	return chk2; /* position of initial value character in pstr */
}
/*----------------------------------------------------------------------------*/
int tdtags_copy(my1todo_tags_t* tags, my1todo_tags_t* from) {
	tags->uuid = from->uuid;
	cstr_assign(&tags->aval,from->aval.buff);
	cstr_assign(&tags->akey,from->akey.buff);
	return 0;
}
/*----------------------------------------------------------------------------*/
void tdtype_free_name(void* that) {
	my1item_t* item = (my1item_t*)that;
	tdname_free((my1todo_name_t*)item->data);
	free(that);
}
/*----------------------------------------------------------------------------*/
void tdtype_init(my1todo_type_t* type) {
	tddata_init((my1todo_data_t*)type);
	list_init(&type->cons,tdtype_free_name);
	list_init(&type->pros,tdtype_free_name);
}
/*----------------------------------------------------------------------------*/
void tdtype_free(my1todo_type_t* type) {
	list_free(&type->pros);
	list_free(&type->cons);
}
/*----------------------------------------------------------------------------*/
my1todo_name_t* tdtype_find(my1list_t* list, char* name) {
	my1todo_name_t *find;
	list_scan_prep(list);
	while (list_scan_item(list)) {
		find = (my1todo_name_t*) list->curr->data;
		if (!strncmp(name,find->name.buff,find->name.fill+1))
			return find;
	}
	return 0x0;
}
/*----------------------------------------------------------------------------*/
my1todo_name_t* tdtype_find_uuid(my1list_t* list, my1todo_name_t* curr) {
	my1todo_name_t *find;
	list_scan_prep(list);
	while (list_scan_item(list)) {
		find = (my1todo_name_t*) list->curr->data;
		if ((find->uuid.date==curr->uuid.date)&&
				(find->uuid.time==curr->uuid.time))
			return find;
	}
	return 0x0;
}
/*----------------------------------------------------------------------------*/
my1todo_name_t* tdtype_make(my1list_t* list, char* name) {
	my1todo_name_t *temp, *find;
	find = tdtype_find(list,name);
	if (find) return find;
	temp = (my1todo_name_t*) malloc(sizeof(my1todo_name_t));
	if (temp) {
		tdname_init(temp);
		tdname_make(temp,name);
		/* ensure unique uuid */
		while (tdtype_find_uuid(list,temp))
			time_add_seconds((my1time_t*)temp,1);
		/* insert sorted list */
		list_scan_prep(list);
		while (list_scan_item(list)) {
			find = (my1todo_name_t*) list->curr->data;
			if (strncmp(name,find->name.buff,find->name.fill+1)<0)
				break;
		}
		list_next_item_data(list,list->curr,(void*)temp);
	}
	return temp;
}
/*----------------------------------------------------------------------------*/
#ifdef _TEST_TODO_DATA_
/*----------------------------------------------------------------------------*/
#include "my1date.c"
#include "my1cstr.c"
#include "my1cstr_line.c"
#include "my1cstr_util.c"
#include "my1strtok.c"
#include "my1list.c"
#include <stdio.h>
/*----------------------------------------------------------------------------*/
#define TODO_FLAG_COMPLETED 0x01
/*----------------------------------------------------------------------------*/
int todo_read(my1todo_data_t* data, char* line) {
	char* look;
	my1cstr_t buff, prio, task, pros, cons;
	my1strtok_t find;
	my1date_time_t made, done, ddue;
	int iret;
	cstr_init(&buff);
	cstr_init(&prio);
	cstr_init(&task);
	cstr_init(&pros);
	cstr_init(&cons);
	/* save local copy */
	cstr_assign(&buff,line);
	strtok_init(&find," ");
	iret = 0;
	do {
		strtok_prep(&find,&buff);
		if (!strtok_next(&find)) {
			iret = -1;
			break;
		}
		/* clear completion flag, priority */
		tddata_flag_clr(data,TODO_FLAG_COMPLETED);
		/* check for priority/complete marks */
		look = find.token.buff;
		if (look[0]=='x'&&find.token.fill==1) {
			tddata_flag_set(data,TODO_FLAG_COMPLETED);
			if (!strtok_next(&find)) {
				iret = -2;
				break;
			}
			look = find.token.buff;
		}
		if (look[0]=='(') {
			if (find.token.fill!=3||look[2]!=')'||look[1]<0x41||look[1]>0x5b) {
				iret = -3;
				break;
			}
			cstr_assign(&prio,"[Priority:");
			cstr_append(&prio,look);
			cstr_append(&prio,"]");
			if (!strtok_next(&find)) {
				iret = -4;
				break;
			}
			look = find.token.buff;
		}
		/* clear dates */
		made.date = DATE_INVALID;
		made.time = TIME_INVALID;
		done.date = DATE_INVALID;
		ddue.date = DATE_INVALID;
		/* check for dates: completion and creation */
		if (look[0]>=0x30&&look[0]<=0x39) {
			/* assume completion date */
			date_isostr((my1date_t*)&done,look);
			if (done.date==DATE_INVALID) {
				iret = -5;
				break;
			}
			if (!strtok_next(&find)) {
				iret = -6;
				break;
			}
			look = find.token.buff;
			/* check if that was actually creation date */
			if (look[0]<0x30||look[0]>0x39) {
				made.date = done.date;
				done.date = DATE_INVALID;
			}
			else {
				date_isostr((my1date_t*)&made,look);
				if (made.date==DATE_INVALID) {
					iret = -7;
					break;
				}
				if (!strtok_next(&find)) {
					iret = -8;
					break;
				}
				look = find.token.buff;
			}
		}
		/* the rest is task - put in residue first */
		while (look||strtok_next(&find)) {
			if (!look) look = find.token.buff;
			cstr_addcol(&task,look,MY1CSTR_PREFIX_SPACING);
			/* check contexts */
			if (look[0]=='@'&&look[1]!=0x0) {
				if (cstr_findwd(&cons,look)<0)
					cstr_addcol(&cons,look,MY1CSTR_PREFIX_SPACING);
			}
			else if (look[0]=='+'&&look[1]!=0x0) {
				if (cstr_findwd(&pros,look)<0)
					cstr_addcol(&pros,look,MY1CSTR_PREFIX_SPACING);
			}
			look = 0x0;
		}
	} while (0);
	if (!iret) {
		if (done.date!=DATE_INVALID) {
			cstr_append(&prio,"[Done@");
			cstr_append(&prio,date_2my1((my1date_t*)&done,0x0));
			cstr_append(&prio,"]");
		}
		if (made.date!=DATE_INVALID) {
			cstr_append(&prio,"[Made@");
			cstr_append(&prio,date_2my1((my1date_t*)&made,0x0));
			cstr_append(&prio,"]");
		}
		if (ddue.date!=DATE_INVALID) {
			cstr_append(&prio,"[Due@");
			cstr_append(&prio,date_2my1((my1date_t*)&ddue,0x0));
			cstr_append(&prio,"]");
		}
		printf("## %s %s\n",tddata_flag_chk(data,TODO_FLAG_COMPLETED)?
			"[DONE]":"[TASK]",task.buff);
		if (prio.fill)
			printf("   > %s\n",prio.buff);
		if (cons.fill)
			printf("   > Context: %s\n",cons.buff);
		if (pros.fill)
			printf("   > Project: %s\n",pros.buff);
	}
	else printf("** Invalid my1TODOtxt format!\n");
	strtok_free(&find);
	cstr_free(&cons);
	cstr_free(&pros);
	cstr_free(&task);
	cstr_free(&prio);
	cstr_free(&buff);
	return iret;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int stat, test, step;
	my1cstr_t buff;
	my1todo_data_t temp;
	FILE* fptr;
	cstr_init(&buff);
	do {
		stat = 0; step = 0;
		if (argc<2) {
			printf("** No input file given!\n");
			stat = -1;
			break;
		}
		fptr = fopen(argv[1],"rt");
		if (!fptr) {
			printf("** Cannot open file '%s'!\n",argv[1]);
			stat = -1;
			break;
		}
		printf("@@ Reading '%s'...\n",argv[1]);
		while ((test=cstr_read_line(&buff,fptr))>=0) {
			step++;
			printf("[Line:%3d] '%s' (%d:%d/%d)\n",step,
				buff.buff,test,buff.fill,buff.size);
			if (buff.fill) todo_read(&temp,buff.buff);
		}
		fclose(fptr);
		printf("@@ Done.\n");
	} while(0);
	cstr_free(&buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif /* _TEST_TODO_DATA_ */
/*----------------------------------------------------------------------------*/
#endif /** __MY1TODO_DATA_C__ */
/*----------------------------------------------------------------------------*/
