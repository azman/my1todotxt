/*----------------------------------------------------------------------------*/
#ifndef __MY1TODO_C__
#define __MY1TODO_C__
/*----------------------------------------------------------------------------*/
#include "todo_data.c"
#include "todo_task.c"
#include "todo_list.c"
/*----------------------------------------------------------------------------*/
#ifdef _TEST_TODO_
/*----------------------------------------------------------------------------*/
#include "my1date.c"
#include "my1cstr.c"
#include "my1cstr_line.c"
#include "my1cstr_util.c"
#include "my1strtok.c"
#include "my1list.c"
#include "my1list_sort.c"
#include "my1text.c"
#include "todo_data.c"
#include "todo_task.c"
#include "todo_debug.h"
#include <stdio.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1todo_task_t *task;
	my1todo_name_t *ptmp;
	my1todo_list_t todo;
	my1list_t temp;
	my1cstr_t buff, tnew;
	my1date_t curr;
	int stat, loop;
	tdlist_init(&todo);
	list_init(&temp,list_free_item); /* pointers only */
	cstr_init(&buff);
	cstr_init(&tnew);
	do {
		stat = 0;
		if (argc<2) {
			fprintf(stderr,"** No input file given!\n");
			stat = -1;
			break;
		}
		if (tdlist_read(&todo,argv[1])) {
			fprintf(stderr,"** ToDo read error! (%d)\n",todo.stat);
			stat = -2;
			break;
		}
		tdlist_task(&todo,&temp);
		printf("@@ Copied list (%d)\n",temp.size);
		for (loop=2;loop<argc;loop++) {
			if (!strcmp(argv[loop],"--due")) {
				if (++loop<argc) {
					date_my1str(&curr,argv[loop]);
					if (curr.date!=DATE_INVALID) {
						filter_task_due(&temp,&curr);
						printf("@@ Filtered list {due:%s}(%d)\n",
							argv[loop],temp.size);
					}
					else fprintf(stderr,"** Invalid date (%s)\n",argv[loop]);
				}
				else fprintf(stderr,"** No date (YYYYMMDD) provided?!\n");
			}
			else if (!strcmp(argv[loop],"--context")) {
				if (++loop<argc) {
					filter_task_context(&temp,argv[loop]);
					printf("@@ Filtered list {context:%s}(%d)\n",
						argv[loop],temp.size);
				}
				else fprintf(stderr,"** No context provided?!\n");
			}
			else if (!strcmp(argv[loop],"--project")) {
				if (++loop<argc) {
					filter_task_project(&temp,argv[loop]);
					printf("@@ Filtered list {project:%s}(%d)\n",
						argv[loop],temp.size);
				}
				else fprintf(stderr,"** No project provided?!\n");
			}
			else if (!strcmp(argv[loop],"--task")) {
				for (++loop;loop<argc;loop++)
					cstr_addcol(&tnew,argv[loop],MY1CSTR_PREFIX_SPACING);
				printf("@@ {NewTask:%s}\n",tnew.buff);
			}
		}
		if (tnew.fill>0) {
			task = tdlist_make_task(&todo,tnew.buff);
			if (!task) {
				fprintf(stderr,"** Cannot make task!(%d:%s)\n",
					todo.code,tnew.buff);
				cstr_null(&tnew);
			}
			else {
				fprintf(stderr,"@@ {AddTask:%s}\n",task->text.buff);
				tdlist_sort(&todo);
				tdlist_task(&todo,&temp);
				printf("@@ Current list (%d)\n",temp.size);
			}
		}
		list_scan_prep(&temp);
		while (list_scan_item(&temp)) {
			task = (my1todo_task_t*) temp.curr->data;
			tdtask_view_str(task,&buff,TDTASK_VIEW_FULL);
			printf(">> %s\n",buff.buff);
		}
		printf("## All Context(s):");
		list_scan_prep(&todo.cons);
		while (list_scan_item(&todo.cons)) {
			ptmp = (my1todo_name_t*)todo.cons.curr->data;
			printf(" %s",ptmp->name.buff);
		}
		printf("\n");
		printf("## All Project(s):");
		list_scan_prep(&todo.pros);
		while (list_scan_item(&todo.pros)) {
			ptmp = (my1todo_name_t*)todo.pros.curr->data;
			printf(" %s",ptmp->name.buff);
		}
		printf("\n");
	} while(0);
	cstr_free(&tnew);
	cstr_free(&buff);
	list_free(&temp);
	tdlist_free(&todo);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif /* _TEST_TODO_ */
/*----------------------------------------------------------------------------*/
#endif /** __MY1TODO_C__ */
/*----------------------------------------------------------------------------*/
