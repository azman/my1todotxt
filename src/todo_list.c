/*----------------------------------------------------------------------------*/
#include "todo_list.h"
/*----------------------------------------------------------------------------*/
#ifndef __MY1TODO_LIST_C__
#define __MY1TODO_LIST_C__
/*----------------------------------------------------------------------------*/
#include "my1cstr_util.h"
#include "my1list_sort.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void tdlist_free_task(void* task) {
	my1item_t* item = (my1item_t*)task;
	tdtask_free((my1todo_task_t*)item->data);
	free(task);
}
/*----------------------------------------------------------------------------*/
void tdlist_init(my1todo_list_t* list) {
	tdtype_init(TDTYPE(list));
	list_init(&list->task,tdlist_free_task);
	text_init(&list->text);
}
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
void tdlist_free(my1todo_list_t* list) {
	text_free(&list->text);
	list_free(&list->task);
	tdtype_free(TDTYPE(list));
}
/*----------------------------------------------------------------------------*/
void tdlist_done(my1todo_list_t* list) {
	tddata_init((my1todo_data_t*)list); /* update uuid */
	list_redo(&list->cons);
	list_redo(&list->pros);
	list_redo(&list->task);
}
/*----------------------------------------------------------------------------*/
my1todo_task_t* tdlist_find_task(my1todo_list_t* list, my1time_t* made) {
	my1todo_task_t *task, *find;
	find = 0x0;
	list_scan_prep(&list->task);
	while (list_scan_item(&list->task)) {
		task = (my1todo_task_t*) list->task.curr->data;
		if (task->uuid.date==made->date&&task->uuid.time==made->time) {
			find = task;
			break;
		}
	}
	return find;
}
/*----------------------------------------------------------------------------*/
my1todo_task_t* tdlist_make_task(my1todo_list_t* list, char* pstr) {
	my1todo_task_t *task;
	task = (my1todo_task_t*) malloc(sizeof(my1todo_task_t));
	tdtask_init(task,(my1todo_type_t*)list);
	list->code = tdtask_read(task,pstr);
	if (list->code) {
		/* cleanup! */
		tdtask_free(task);
		free((void*)task);
		return 0x0;
	}
	/* make sure made is unique */
	while (tdlist_find_task(list,(my1time_t*)&task->uuid))
		time_add_seconds((my1time_t*)&task->uuid,1);
	/* add to list */
	list_push_item_data(&list->task,task);
	return task;
}
/*----------------------------------------------------------------------------*/
int tdlist_read(my1todo_list_t* list, char* name) {
	if (list->task.size>0) tdlist_done(list);
	list->stat = 0;
	text_open(&list->text,name);
	if (!list->text.pfile) {
		list->stat = TDLIST_ERROR_FOPEN;
		return list->stat;
	}
	list->code = 0;
	while (text_read(&list->text)>=CHAR_INIT) {
		if (!list->text.pbuff.fill) continue;
		if (!tdlist_make_task(list,list->text.pbuff.buff)) {
			list->stat = TDLIST_ERROR_TMAKE;
			break;
		}
	}
	text_done(&list->text);
	return list->stat;
}
/*----------------------------------------------------------------------------*/
int tdtask_compare(void* pchk, void* qchk) {
	my1todo_task_t *pdat = (my1todo_task_t*) pchk;
	my1todo_task_t *qdat = (my1todo_task_t*) qchk;
	return strcmp(pdat->text.buff,qdat->text.buff);
}
/*----------------------------------------------------------------------------*/
int tdtask_compare_due(void* pchk, void* qchk) {
	my1todo_task_t *pdat = (my1todo_task_t*) pchk;
	my1todo_task_t *qdat = (my1todo_task_t*) qchk;
	if (pdat->ddue.date==DATE_INVALID||qdat->ddue.date==DATE_INVALID) {
		if (pdat->ddue.date!=DATE_INVALID) /* qdat is invalid */
			return -1;
		if (qdat->ddue.date!=DATE_INVALID) /* pdat is invalid */
			return 1;
		return 0; /* both invalid */
	}
	return date_subtract(&pdat->ddue,&qdat->ddue);
}
/*----------------------------------------------------------------------------*/
int tdlist_sort(my1todo_list_t* list) {
	my1list_t temp;
	int diff = 0; /* no position changes made */
	tdlist_sortdiff0(list);
	list_init(&temp,list_free_item);
	/* copy current position to temp list */
	list_scan_prep(&list->task);
	while (list_scan_item(&list->task))
		list_push_item_data(&temp,list->task.curr->data);
	/* do the sort */
	list_sort(&list->task,tdtask_compare);
	/* check if position changed */
	temp.curr = temp.init;
	list_scan_prep(&list->task);
	while (list_scan_item(&list->task)) {
		if (temp.curr->data!=list->task.curr->data) {
			diff++;
			break;
		}
		temp.curr = temp.curr->next;
	}
	list_free(&temp);
	if (diff) {
		tdlist_modify(list);
		tdlist_sortdiff0(list);
	}
	return diff;
}
/*----------------------------------------------------------------------------*/
int tdlist_prep(my1todo_list_t* list) {
	my1todo_task_t* task;
	int test = 0;
	tdlist_prepdiff0(list);
	list_scan_prep(&list->task);
	while (list_scan_item(&list->task)) {
		task = (my1todo_task_t*) list->task.curr->data;
		tdtask_prep(task);
		if (strcmp(task->text.buff,task->prep.buff))
			test++;
	}
	if (test) tdlist_prepdiff1(list);
	return test;
}
/*----------------------------------------------------------------------------*/
int tdlist_prep2text(my1todo_list_t* list) {
	my1todo_task_t* task;
	list_scan_prep(&list->task);
	while (list_scan_item(&list->task)) {
		task = (my1todo_task_t*)list->task.curr->data;
		cstr_assign(&task->text,task->prep.buff);
	}
	if (tdlist_prepdiff(list))
		tdlist_modify(list);
	return list->stat;
}
/*----------------------------------------------------------------------------*/
int tdlist_save(my1todo_list_t* list, char* name) {
	my1todo_task_t* task;
	FILE *pchk, *ptmp;
	ptmp = fopen(name,"wt");
	if (!ptmp) {
		list->stat |= TDLIST_ERROR_FOPEN;
		pchk = stderr;
	}
	else pchk = ptmp;
	list_scan_prep(&list->task);
	while (list_scan_item(&list->task)) {
		task = (my1todo_task_t*)list->task.curr->data;
		fprintf(pchk,"%s\n",task->text.buff);
	}
	if (ptmp) {
		fclose(ptmp);
		tdlist_update(list); /* remove modified flag */
	}
	return list->stat;
}
/*----------------------------------------------------------------------------*/
int tdlist_task(my1todo_list_t* list, my1list_t* copy) {
	list_redo(copy); /* should be default list with list_free_item */
	list_scan_prep(&list->task);
	while (list_scan_item(&list->task))
		list_push_item_data(copy,list->task.curr->data);
	return copy->size;
}
/*----------------------------------------------------------------------------*/
int filter_task_context(my1list_t* list, char* acon) {
	my1todo_task_t* task;
	list->curr = list->init;
	while (list->curr) {
		task = (my1todo_task_t*)list->curr->data;
		if (!tdtask_find_cons(task,acon))
			list_hack_item(list,list->curr);
		else list->curr = list->curr->next;
	}
	return list->size;
}
/*----------------------------------------------------------------------------*/
int filter_task_project(my1list_t* list, char* apro) {
	my1todo_task_t* task;
	list->curr = list->init;
	while (list->curr) {
		task = (my1todo_task_t*)list->curr->data;
		if (!tdtask_find_pros(task,apro))
			list_hack_item(list,list->curr);
		else list->curr = list->curr->next;
	}
	return list->size;
}
/*----------------------------------------------------------------------------*/
int filter_task_due(my1list_t* list, my1date_t* curr) {
	my1todo_task_t* task;
	list->curr = list->init;
	while (list->curr) {
		task = (my1todo_task_t*)list->curr->data;
		if (tdtask_calc_due(task,curr)==TDTASK_DUE_NONE)
			list_hack_item(list,list->curr);
		else list->curr = list->curr->next;
	}
	/* rearrange based on due date! */
	if (list->size>1) list_sort(list,tdtask_compare_due);
	return list->size;
}
/*----------------------------------------------------------------------------*/
#ifdef _TEST_TODO_LIST_
/*----------------------------------------------------------------------------*/
#include "my1date.c"
#include "my1cstr.c"
#include "my1cstr_line.c"
#include "my1cstr_util.c"
#include "my1strtok.c"
#include "my1list.c"
#include "my1list_sort.c"
#include "my1text.c"
#include "todo_data.c"
#include "todo_task.c"
#include "todo_debug.h"
#include <stdio.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1cstr_t buff;
	my1todo_list_t todo;
	my1todo_task_t *task;
	my1todo_name_t *ptmp;
	int stat;
	cstr_init(&buff);
	tdlist_init(&todo);
	do {
		stat = 0;
		if (argc<2) {
			fprintf(stderr,"** No input file given!\n");
			stat = -1;
			break;
		}
		if (tdlist_read(&todo,argv[1])) {
			fprintf(stderr,"** ToDoList read error! (%d)\n",todo.stat);
			stat = -2;
			break;
		}
		list_scan_prep(&todo.task);
		while (list_scan_item(&todo.task)) {
			task = (my1todo_task_t*) todo.task.curr->data;
			tdtask_view_str(task,&buff,TDTASK_VIEW_FULL);
			printf(">> %s\n",buff.buff);
			todo_show_task(task);
		}
		printf("## All Context(s):");
		list_scan_prep(&todo.cons);
		while (list_scan_item(&todo.cons)) {
			ptmp = (my1todo_name_t*)todo.cons.curr->data;
			printf(" %s",ptmp->name.buff);
		}
		printf("\n");
		printf("## All Project(s):");
		list_scan_prep(&todo.pros);
		while (list_scan_item(&todo.pros)) {
			ptmp = (my1todo_name_t*)todo.pros.curr->data;
			printf(" %s",ptmp->name.buff);
		}
		printf("\n");
	} while(0);
	tdlist_free(&todo);
	cstr_free(&buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif /* _TEST_TODO_LIST_ */
/*----------------------------------------------------------------------------*/
#endif /** __MY1TODO_LIST_C__ */
/*----------------------------------------------------------------------------*/
