/*----------------------------------------------------------------------------*/
#ifndef __MY1TODO_LIST_H__
#define __MY1TODO_LIST_H__
/*----------------------------------------------------------------------------*/
#include "todo_task.h"
#include "my1text.h"
/*----------------------------------------------------------------------------*/
/* flag for list */
#define TDLIST_FLAG_MODIFIED 0x01
#define TDLIST_FLAG_PREPDIFF 0x02
#define TDLIST_FLAG_SORTDIFF 0x04
/*----------------------------------------------------------------------------*/
typedef struct _my1todo_list_t {
	my1date_time_t uuid;
	my1list_t cons; /* list of my1todo_name_t (contexts) */
	my1list_t pros; /* list of my1todo_name_t (projects) */
	/* up until here is my1todo_type_t */
	my1list_t task; /* list of my1todo_task_t */
	my1text_t text; /* raw text from file */
	unsigned int stat, code; /* error status and code */
} my1todo_list_t;
/*----------------------------------------------------------------------------*/
#define TDLIST_ERROR_FMISS 0x0001
#define TDLIST_ERROR_FOPEN 0x0002
#define TDLIST_ERROR_TMAKE 0x0004
/*----------------------------------------------------------------------------*/
void tdlist_init(my1todo_list_t* list);
void tdlist_free(my1todo_list_t* list);
void tdlist_done(my1todo_list_t* list); /* reset for next read */
my1todo_task_t* tdlist_find_task(my1todo_list_t* list, my1time_t* made);
my1todo_task_t* tdlist_make_task(my1todo_list_t* list, char* pstr);
int tdlist_read(my1todo_list_t* list, char* name);
int tdlist_sort(my1todo_list_t* list);
int tdlist_prep(my1todo_list_t* list);
int tdlist_prep2text(my1todo_list_t* list);
int tdlist_save(my1todo_list_t* list, char* name);
int tdlist_task(my1todo_list_t* list, my1list_t* copy);
int filter_task_context(my1list_t* list, char* acon);
int filter_task_project(my1list_t* list, char* apro);
int filter_task_due(my1list_t* list, my1date_t* curr);
/*----------------------------------------------------------------------------*/
/** useful macro */
#define tdlist_modify(pl) tddata_flag_set(TDDATA(pl),TDLIST_FLAG_MODIFIED)
#define tdlist_update(pl) tddata_flag_clr(TDDATA(pl),TDLIST_FLAG_MODIFIED)
#define tdlist_modified(pl) tddata_flag_get(TDDATA(pl),TDLIST_FLAG_MODIFIED)
#define tdlist_prepdiff1(pl) tddata_flag_set(TDDATA(pl),TDLIST_FLAG_PREPDIFF)
#define tdlist_prepdiff0(pl) tddata_flag_clr(TDDATA(pl),TDLIST_FLAG_PREPDIFF)
#define tdlist_prepdiff(pl) tddata_flag_get(TDDATA(pl),TDLIST_FLAG_PREPDIFF)
#define tdlist_sortdiff1(pl) tddata_flag_set(TDDATA(pl),TDLIST_FLAG_SORTDIFF)
#define tdlist_sortdiff0(pl) tddata_flag_clr(TDDATA(pl),TDLIST_FLAG_SORTDIFF)
#define tdlist_sortdiff(pl) tddata_flag_get(TDDATA(pl),TDLIST_FLAG_SORTDIFF)
/*----------------------------------------------------------------------------*/
#endif /** __MY1TODO_LIST_H__ */
/*----------------------------------------------------------------------------*/
