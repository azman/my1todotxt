/*----------------------------------------------------------------------------*/
#ifndef __MY1DATA_H__
#define __MY1DATA_H__
/*----------------------------------------------------------------------------*/
#include "todo.h"
#include "my1path.h"
#include "my1type.h"
/*----------------------------------------------------------------------------*/
#define STAT_EMASK ~STAT_ERROR
#define STAT_EFLAG 0x00000001
#define STAT_ERROR_LOAD (STAT_ERROR|(STAT_EFLAG<<0))
#define STAT_ERROR_SAVE (STAT_ERROR|(STAT_EFLAG<<1))
/* last used stat bit */
#define STAT_EFLAG_LAST (STAT_EMASK&STAT_ERROR_SAVE)
/*----------------------------------------------------------------------------*/
#define FLAG_AFLAG 0x00000001
/* data_init checks this */
#define FLAG_MY1FORM (FLAG_AFLAG<<0)
/* data_pass checks this */
#define FLAG_LISTDUE (FLAG_AFLAG<<1)
/* last used flag bit */
#define FLAG_LFLAG FLAG_LISTDUE
/*----------------------------------------------------------------------------*/
#ifndef DEFAULT_TODOTXT
#define DEFAULT_TODOTXT "todo.txt"
#endif
/*----------------------------------------------------------------------------*/
typedef struct _data_t {
	my1todo_list_t todo;
	my1list_t tfil, *show;
	my1path_t name;
	my1cstr_t buff;
	/* main interface with top level */
	int stat, flag;
	/* useful temporary storage */
	int loop, temp;
	my1time_t uuid;
	my1todo_task_t* task;
	char *psrc, *pcon, *ppro;
	char dstr[16]; /* DATETIME_MY1FORMAT @ DATETIME_MY1FORMAT_LEN */
} data_t;
/*----------------------------------------------------------------------------*/
void data_init(data_t* data);
void data_free(data_t* data);
int data_path(data_t* data, char* name);
int data_load(data_t* data, char* name);
int data_save(data_t* data, char* name);
int data_pass(data_t* data, char* pcon, char* ppro);
int data_todo(data_t* data);
/*----------------------------------------------------------------------------*/
#endif /** __MY1DATA_H__ */
/*----------------------------------------------------------------------------*/
