/*----------------------------------------------------------------------------*/
#include "data.h"
/*----------------------------------------------------------------------------*/
void data_init(data_t* data) {
	tdlist_init(&data->todo);
	list_init(&data->tfil,list_free_item);
	data->show = &data->todo.task;
	path_init(&data->name);
	cstr_init(&data->buff);
}
/*----------------------------------------------------------------------------*/
void data_free(data_t* data) {
	cstr_free(&data->buff);
	path_free(&data->name);
	list_free(&data->tfil);
	tdlist_free(&data->todo);
}
/*----------------------------------------------------------------------------*/
int data_path(data_t* data, char* name) {
	data->name.trap = 0; /* reset error */
	path_access(&data->name,name);
	if (data->name.trap||!(data->name.flag&PATH_FLAG_FILE))
		return 1; /* read error or NOT an existing file */
	return 0;
}
/*----------------------------------------------------------------------------*/
int data_load(data_t* data, char* name) {
	if (tdlist_read(&data->todo,name)) {
		data->stat |= STAT_ERROR_LOAD;
		fprintf(stderr,"** ToDoList read error! (%d)\n",data->todo.stat);
	}
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_save(data_t* data, char* name) {
	if (!data->todo.task.size) return data->stat;
	if (tdlist_save(&data->todo,name)) {
		data->stat |= STAT_ERROR_SAVE;
		fprintf(stderr,"** ToDoList save error! (%d)\n",data->todo.stat);
	}
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_pass(data_t* data, char* pcon, char* ppro) {
	my1time_t time;
	tdlist_task(&data->todo,&data->tfil);
	if (pcon) filter_task_context(&data->tfil,pcon);
	if (ppro) filter_task_project(&data->tfil,ppro);
	if (data->flag&FLAG_LISTDUE) {
		time_read(&time,1);
		filter_task_due(&data->tfil,(my1date_t*)&time);
	}
	data->show = &data->tfil;
	return data->tfil.size;
}
/*----------------------------------------------------------------------------*/
int data_todo(data_t* data) {
	if (data->flag&FLAG_MY1FORM) {
		tdlist_prep(&data->todo);
		tdlist_prep2text(&data->todo);
	}
	tdlist_sort(&data->todo);
	/* returns non-zero if changes occur */
	return tdlist_sortdiff(&data->todo)||tdlist_prepdiff(&data->todo);
}
/*----------------------------------------------------------------------------*/
