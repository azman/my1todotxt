/*----------------------------------------------------------------------------*/
#include "gtkapp.h"
#include "my1cstr_util.h"
/*----------------------------------------------------------------------------*/
void gtkapp_init_data(my1gtkapp_t* gapp, int argc, char* argv[]) {
	int loop;
	cstr_init(&gapp->nbuf);
	cstr_init(&gapp->tbuf);
	cstr_init(&gapp->ibuf);
	gapp->pick = 0x0;
	gapp->preq = 0x0;
	MY1DATA(gapp)->stat = 0;
	MY1DATA(gapp)->flag = 0;
	for (loop=1;loop<argc;loop++) {
		if (argv[loop][0]=='-') {
			if (!strncmp(&argv[loop][1],"-full",5))
				MY1DATA(gapp)->flag |= FLAG_DO_FULL;
			else if (!strncmp(&argv[loop][1],"-uuid",5))
				MY1DATA(gapp)->flag |= FLAG_CHKUUID;
			else fprintf(stderr,"** Unknown option '%s'!\n",argv[loop]);
		}
		else {
			if (gapp->preq) {
				fprintf(stderr,"** Multiple name specified! (%s|%s)\n",
					argv[loop],gapp->preq);
			}
			else gapp->preq = argv[loop];
		}
	}
	gapp->psrc = gapp->preq?gapp->preq:DEFAULT_TODOTXT;
	data_init(MY1DATA(gapp));
}
/*----------------------------------------------------------------------------*/
void gtkapp_free_data(my1gtkapp_t* gapp) {
	data_free(MY1DATA(gapp));
	cstr_free(&gapp->ibuf);
	cstr_free(&gapp->tbuf);
	cstr_free(&gapp->nbuf);
}
/*----------------------------------------------------------------------------*/
char* gtkapp_get_filename(my1gtkapp_t* gapp) {
	char* pstr = (char*)gtk_header_bar_get_subtitle((GtkHeaderBar*)gapp->hbar);
	if (!pstr||pstr[0]!='*') return pstr;
	return &pstr[1];
}
/*----------------------------------------------------------------------------*/
void gtkapp_set_filename(my1gtkapp_t* gapp, char* name) {
	gtk_header_bar_set_subtitle((GtkHeaderBar*)gapp->hbar,name);
}
/*----------------------------------------------------------------------------*/
void on_task_menu_priority1(GtkWidget *widget, gpointer data) {
	int skip = 0;
	my1gtkapp_t* gapp = (my1gtkapp_t*) data;
	gapp->task = (my1todo_task_t*)
		g_object_get_data(G_OBJECT(widget),MY1TASK_KEY);
	if (gapp->task->prio.fill>0) {
		if (gapp->task->prio.buff[1]>'A')
			gapp->task->prio.buff[1]--;
		else {
			fprintf(stderr,"@@ Priority at max %s!\n",gapp->task->prio.buff);
			skip = 1;
		}
	}
	else cstr_setstr(&gapp->task->prio,"(A)");
	if (!skip) {
		tdtask_prep(gapp->task);
		cstr_assign(&gapp->task->text,gapp->task->prep.buff);
		gtkapp_data_update(gapp);
		gtkapp_todo_modified(gapp,1);
	}
}
/*----------------------------------------------------------------------------*/
void on_task_menu_priority0(GtkWidget *widget, gpointer data) {
	my1gtkapp_t* gapp = (my1gtkapp_t*) data;
	gapp->task = (my1todo_task_t*)
		g_object_get_data(G_OBJECT(widget),MY1TASK_KEY);
	if (gapp->task->prio.fill>0) {
		if (gapp->task->prio.buff[1]<'Z') {
			gapp->task->prio.buff[1]++;
			tdtask_prep(gapp->task);
			cstr_assign(&gapp->task->text,gapp->task->prep.buff);
			gtkapp_data_update(gapp);
			gtkapp_todo_modified(gapp,1);
		}
		else fprintf(stderr,"@@ Priority at min %s!\n",gapp->task->prio.buff);
	}
	else fprintf(stderr,"** Should not be here! [on_task_menu_priority0]\n");
}
/*----------------------------------------------------------------------------*/
void on_task_menu_priorityX(GtkWidget *widget, gpointer data) {
	my1gtkapp_t* gapp = (my1gtkapp_t*) data;
	gapp->task = (my1todo_task_t*)
		g_object_get_data(G_OBJECT(widget),MY1TASK_KEY);
	if (gapp->task->prio.fill>0) {
		cstr_null(&gapp->task->prio);
		tdtask_prep(gapp->task);
		cstr_assign(&gapp->task->text,gapp->task->prep.buff);
		gtkapp_data_update(gapp);
		gtkapp_todo_modified(gapp,1);
	}
	else fprintf(stderr,"** Should not be here! [on_task_menu_priorityX]\n");
}
/*----------------------------------------------------------------------------*/
void on_task_menu_completed(GtkWidget *widget, gpointer data) {
	my1gtkapp_t* gapp = (my1gtkapp_t*) data;
	gapp->task = (my1todo_task_t*)
		g_object_get_data(G_OBJECT(widget),MY1TASK_KEY);
	if (tdtask_done(gapp->task)<0) {
		fprintf(stderr,"** Failed to mark [%08d%06d] complete!\n",
			gapp->task->uuid.date,gapp->task->uuid.time);
	}
	else {
		tdtask_prep(gapp->task);
		cstr_assign(&gapp->task->text,gapp->task->prep.buff);
		gtkapp_data_update(gapp);
		gtkapp_todo_modified(gapp,1);
	}
}
/*----------------------------------------------------------------------------*/
gboolean on_task_clicked(GtkWidget *widget,
		GdkEventButton *event, gpointer data) {
	int test,loop;
	GList *item, *init;
	my1gtkapp_t* gapp = (my1gtkapp_t*) data;
	gboolean done = FALSE;
	if (event->type == GDK_DOUBLE_BUTTON_PRESS) {
		if (event->button == 1) /* L/M/R => 1/2/3*/ {
			gapp->task = (my1todo_task_t*)
				g_object_get_data(G_OBJECT(widget),MY1TASK_KEY);
			/* filter out uid: tag from task */
			cstr_setstr(&gapp->tbuf,gapp->task->task.buff);
			test = cstr_findwd(&gapp->tbuf,"uid:");
			if (test>=0) {
				loop = cstr_rmword(&gapp->tbuf,"uid:"," ");
				cstr_substr(&gapp->ibuf,&gapp->task->task,test,loop);
				cstr_trimws(&gapp->ibuf,1);
				/*fprintf(stderr,"@@ UID:{%s}\n",gapp->ibuf.buff);*/
			}
			gtk_entry_set_text((GtkEntry*)gapp->texe,gapp->tbuf.buff);
			gapp->pick = widget;
			gtk_widget_set_sensitive(gapp->hbar,FALSE);
			gtk_widget_set_sensitive(gapp->bfil,FALSE);
			gtk_widget_set_sensitive(gapp->swin,FALSE);
			done = TRUE;
		}
	}
	else if (event->type == GDK_BUTTON_PRESS) {
		if (event->button == 3) {
			gapp->task = (my1todo_task_t*)
				g_object_get_data(G_OBJECT(widget),MY1TASK_KEY);
			if (tdtask_completed(gapp->task))
				return done;
			/* remove old menu items */
			init = gtk_container_get_children(GTK_CONTAINER(gapp->menu));
			for (item=init;item!=NULL;item=item->next)
				gtk_widget_destroy(GTK_WIDGET(item->data));
			g_list_free(init);
			/* create uuid menu item (no-action) */
/*
			cstr_setstr(&MY1DATA(gapp)->buff,"UUID:");
			cstr_append(&MY1DATA(gapp)->buff,
				time_2my1_full((my1time_t*)&gapp->task->uuid,0x0));
			gapp->wtmp = gtk_menu_item_new_with_label(MY1DATA(gapp)->buff.buff);
			gtk_widget_show(gapp->wtmp);
			gtk_menu_shell_append(GTK_MENU_SHELL(gapp->menu),gapp->wtmp);
			gapp->wtmp = gtk_separator_menu_item_new();
			gtk_menu_shell_append(GTK_MENU_SHELL(gapp->menu),gapp->wtmp);
			gtk_widget_show(gapp->wtmp);
*/
			/* priority */
			if (gapp->task->prio.fill>0) {
				/* increase priority command */
				gapp->wtmp = gtk_menu_item_new_with_label("Increase Priority");
				g_object_set_data(G_OBJECT(gapp->wtmp),MY1TASK_KEY,gapp->task);
				gtk_widget_show(gapp->wtmp);
				gtk_menu_shell_append(GTK_MENU_SHELL(gapp->menu),gapp->wtmp);
				g_signal_connect(G_OBJECT(gapp->wtmp),"activate",
					G_CALLBACK(on_task_menu_priority1),(gpointer)gapp);
				/* decrease priority command */
				gapp->wtmp = gtk_menu_item_new_with_label("Decrease Priority");
				g_object_set_data(G_OBJECT(gapp->wtmp),MY1TASK_KEY,gapp->task);
				gtk_widget_show(gapp->wtmp);
				gtk_menu_shell_append(GTK_MENU_SHELL(gapp->menu),gapp->wtmp);
				g_signal_connect(G_OBJECT(gapp->wtmp),"activate",
					G_CALLBACK(on_task_menu_priority0),(gpointer)gapp);
				/* remove priority command */
				gapp->wtmp = gtk_menu_item_new_with_label("Remove Priority");
				g_object_set_data(G_OBJECT(gapp->wtmp),MY1TASK_KEY,gapp->task);
				gtk_widget_show(gapp->wtmp);
				gtk_menu_shell_append(GTK_MENU_SHELL(gapp->menu),gapp->wtmp);
				g_signal_connect(G_OBJECT(gapp->wtmp),"activate",
					G_CALLBACK(on_task_menu_priorityX),(gpointer)gapp);
			}
			else {
				/* add priority command */
				gapp->wtmp = gtk_menu_item_new_with_label("Add Priority");
				g_object_set_data(G_OBJECT(gapp->wtmp),MY1TASK_KEY,gapp->task);
				gtk_widget_show(gapp->wtmp);
				gtk_menu_shell_append(GTK_MENU_SHELL(gapp->menu),gapp->wtmp);
				g_signal_connect(G_OBJECT(gapp->wtmp),"activate",
					G_CALLBACK(on_task_menu_priority1),(gpointer)gapp);
			}
			/* separator */
			gapp->wtmp = gtk_separator_menu_item_new();
			gtk_menu_shell_append(GTK_MENU_SHELL(gapp->menu),gapp->wtmp);
			gtk_widget_show(gapp->wtmp);
			/* complete command */
			gapp->wtmp = gtk_menu_item_new_with_label("Completed");
			g_object_set_data(G_OBJECT(gapp->wtmp),MY1TASK_KEY,gapp->task);
			gtk_widget_show(gapp->wtmp);
			gtk_menu_shell_append(GTK_MENU_SHELL(gapp->menu),gapp->wtmp);
			g_signal_connect(G_OBJECT(gapp->wtmp),"activate",
				G_CALLBACK(on_task_menu_completed),(gpointer)gapp);
			/* show it! */
			gtk_menu_popup_at_pointer(GTK_MENU(gapp->menu),0x0);
			done = TRUE;
		}
	}
	return done;
}
/*----------------------------------------------------------------------------*/
void gtkapp_pass_show(my1gtkapp_t* gapp) {
	my1list_t* list;
	gchar* pcon =
		gtk_combo_box_text_get_active_text((GtkComboBoxText*)gapp->cncb);
	if (pcon&&pcon[0]==0x0) pcon = 0x0;
	gchar* ppro =
		gtk_combo_box_text_get_active_text((GtkComboBoxText*)gapp->pjcb);
	if (ppro&&ppro[0]==0x0) ppro = 0x0;
/*
	fprintf(stderr,"@@ PASS-PCON:'%s'\n",pcon?pcon:"None");
	fprintf(stderr,"@@ PASS-PPRO:'%s'\n",ppro?ppro:"None");
*/
	/* filter task */
	data_pass(MY1DATA(gapp),pcon,ppro);
	g_free(pcon); g_free(ppro);
/*
	fprintf(stderr,"@@ PASS-SIZE:%d/%d\n",
		MY1DATA(gapp)->show->size,MY1DATA(gapp)->show->size);
*/
	/* clear task list */
	if (gapp->vbox) gtk_widget_destroy(gapp->vbox);
	/* create new container */
	gapp->vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL,0); /* zero spacing */
	gtk_widget_set_valign(gapp->vbox,GTK_ALIGN_START); /* top-align view */
	/* update view from data->todo */
	gapp->temp = 0;
	if (MY1DATA(gapp)->flag&FLAG_CHKUUID) gapp->temp |= TDTASK_VIEW_UUID;
	if (MY1DATA(gapp)->flag&FLAG_DO_FULL) gapp->temp |= TDTASK_VIEW_FULL;
	list = MY1DATA(gapp)->show;
	list_scan_prep(list);
	while (list_scan_item(list)) {
		gapp->task = (my1todo_task_t*) list->curr->data;
		gapp->wtmp = gtk_label_new(NULL);
		/* add markup test */
		cstr_null(&MY1DATA(gapp)->buff);
		cstr_append(&MY1DATA(gapp)->buff,MY1FONT_MONO_INIT);
		if (tdtask_completed(gapp->task))
			cstr_append(&MY1DATA(gapp)->buff,MY1FONT_DONE_INIT);
		tdtask_view_str(gapp->task,&gapp->tbuf,gapp->temp);
		cstr_append(&MY1DATA(gapp)->buff,gapp->tbuf.buff);
		if (tdtask_completed(gapp->task))
			cstr_append(&MY1DATA(gapp)->buff,MY1FONT_DONE_ENDS);
		cstr_append(&MY1DATA(gapp)->buff,MY1FONT_MONO_ENDS);
		gtk_label_set_markup(GTK_LABEL(gapp->wtmp),MY1DATA(gapp)->buff.buff);
		gtk_label_set_xalign(GTK_LABEL(gapp->wtmp),0.0f);
		/* prepare tooltip */
/*
		cstr_setstr(&MY1DATA(gapp)->buff,MY1FONT_BOLD_INIT);
		cstr_append(&MY1DATA(gapp)->buff,"UUID:");
		cstr_append(&MY1DATA(gapp)->buff,MY1FONT_BOLD_ENDS);
		cstr_append(&MY1DATA(gapp)->buff,
			time_2my1_full((my1time_t*)&gapp->task->uuid,0x0));
		g_object_set(G_OBJECT(gapp->wtmp),"has-tooltip",TRUE,NULL);
		g_signal_connect(G_OBJECT(gapp->wtmp),"query-tooltip",
			G_CALLBACK(task_show_tooltip),(gpointer)gapp);
		gtk_widget_set_tooltip_markup(gapp->wtmp,MY1DATA(gapp)->buff.buff);
*/
		/* setup line wrap */
		gtk_label_set_line_wrap(GTK_LABEL(gapp->wtmp),TRUE);
		gtk_label_set_line_wrap_mode(GTK_LABEL(gapp->wtmp),PANGO_WRAP_WORD);
		/* keep reference */
		gapp->task->vobj = (void*) gapp->wtmp;
		/* create a frame */
		gapp->wtmp = gtk_frame_new(NULL);
		gtk_container_add(GTK_CONTAINER(gapp->wtmp),
			GTK_WIDGET(gapp->task->vobj));
		/* keep updated reference */
		gapp->task->vobj = (void*) gapp->wtmp;
		/* create an eventbox */
		gapp->wtmp = gtk_event_box_new();
		gtk_container_add(GTK_CONTAINER(gapp->wtmp),
			GTK_WIDGET(gapp->task->vobj));
		/* create onclick event */
		gtk_widget_add_events(gapp->wtmp,GDK_BUTTON_PRESS_MASK);
		g_signal_connect(G_OBJECT(gapp->wtmp),"button-press-event",
			G_CALLBACK(on_task_clicked),(gpointer)gapp);
		/* keep updated reference */
		gapp->task->vobj = (void*) gapp->wtmp;
		g_object_set_data(G_OBJECT(gapp->wtmp),MY1TASK_KEY,gapp->task);
		/* pack into box (expand=FALSE,fill=TRUE,spacing=2 */
		gtk_box_pack_start(GTK_BOX(gapp->vbox),gapp->wtmp,0,1,2);
	}
	/* put box into view */
	gtk_container_add(GTK_CONTAINER(gapp->view),gapp->vbox);
	gtk_widget_show_all(gapp->view);
}
/*----------------------------------------------------------------------------*/
void gtkapp_data_update(my1gtkapp_t* gapp) {
	my1list_t *list;
	/* update context list */
	gtk_combo_box_text_remove_all((GtkComboBoxText*)gapp->cncb);
	list = &MY1DATA(gapp)->todo.cons;
	list_scan_prep(list);
	while (list_scan_item(list)) {
		gapp->name = (my1todo_name_t*) list->curr->data;
		gtk_combo_box_text_append((GtkComboBoxText*)gapp->cncb,
			tddata_uuid((my1todo_data_t*)gapp->name),gapp->name->name.buff);
	}
	/* update project list */
	gtk_combo_box_text_remove_all((GtkComboBoxText*)gapp->pjcb);
	list = &MY1DATA(gapp)->todo.pros;
	list_scan_prep(list);
	while (list_scan_item(list)) {
		gapp->name = (my1todo_name_t*) list->curr->data;
		gtk_combo_box_text_append((GtkComboBoxText*)gapp->pjcb,
			tddata_uuid((my1todo_data_t*)gapp->name),gapp->name->name.buff);
	}
	/* always sorted? */
	tdlist_sort(&MY1DATA(gapp)->todo);
	/* filter/show task */
	gtkapp_pass_show(gapp);
}
/*----------------------------------------------------------------------------*/
void gtkapp_load_data(my1gtkapp_t* gapp, char* name) {
	if (!data_load(MY1DATA(gapp),name)) {
		/* reset filter option & hide box_filter */
		gtk_toggle_tool_button_set_active(
			(GtkToggleToolButton*)gapp->tfil,FALSE);
		gtk_widget_hide(gapp->bfil);
		/* update data view */
		gtkapp_data_update(gapp);
		/* update titlebar */
		gtkapp_set_filename(gapp,MY1DATA(gapp)->name.full);
		/* clear modified flag */
		tdlist_update((my1todo_data_t*)&MY1DATA(gapp)->todo);
	}
	/* reset load request! */
	gapp->preq = 0x0;
}
/*----------------------------------------------------------------------------*/
void gtkapp_todo_modified(my1gtkapp_t* gapp, int yes) {
	char* pstr;
	if (yes) {
		tdlist_modify((my1todo_data_t*)&MY1DATA(gapp)->todo);
		pstr = gtkapp_get_filename(gapp);
		if (pstr) {
			cstr_assign(&MY1DATA(gapp)->buff,pstr);
			cstr_prefix(&MY1DATA(gapp)->buff,"*");
			gtkapp_set_filename(gapp,MY1DATA(gapp)->buff.buff);
		}
	}
	else {
		char* pstr = (char*)
			gtk_header_bar_get_subtitle((GtkHeaderBar*)gapp->hbar);
		if (!pstr) cstr_assign(&MY1DATA(gapp)->buff,MY1DATA(gapp)->name.full);
		else if (pstr[0]=='*') cstr_assign(&MY1DATA(gapp)->buff,&pstr[1]);
		else cstr_assign(&MY1DATA(gapp)->buff,pstr);
		gtkapp_set_filename(gapp,MY1DATA(gapp)->buff.buff);
		tdlist_update((my1todo_data_t*)&MY1DATA(gapp)->todo);
	}
}
/*----------------------------------------------------------------------------*/
void gtkapp_todo_file_sort(my1gtkapp_t* gapp) {
	int chk1, chk2;
	if (MY1DATA(gapp)->todo.task.size<=0) {
		fprintf(stderr,"@@ Nothing to sort.\n");
		return;
	}
	chk1 = tdlist_prep(&MY1DATA(gapp)->todo);
	tdlist_prep2text(&MY1DATA(gapp)->todo);
	chk2 = tdlist_sort(&MY1DATA(gapp)->todo);
	if (chk1||chk2) {
		gtkapp_data_update(gapp);
		gtkapp_todo_modified(gapp,1);
	}
	/*else fprintf(stderr,"@@ No changes?\n");*/
}
/*----------------------------------------------------------------------------*/
void gtkapp_save_data(my1gtkapp_t* gapp, char* name) {
	gtkapp_todo_file_sort(gapp); /* always sort things out before saving */
	switch (data_save(MY1DATA(gapp),name)) {
		case -1:
			fprintf(stderr,"** Error saving '%s'! (%d)\n",
				MY1DATA(gapp)->name.full,MY1DATA(gapp)->todo.stat);
			break;
		case 0: /* ok */
			gtkapp_todo_modified(gapp,0);
			break;
		default:
			fprintf(stderr,"** Unknown save_data error!\n");
			break;
	}
}
/*----------------------------------------------------------------------------*/
void gtkapp_todo_file_open(my1gtkapp_t* gapp) {
	GtkWidget *doopen = gtk_file_chooser_dialog_new("Open ToDo.txt File",
		GTK_WINDOW(gapp->main),GTK_FILE_CHOOSER_ACTION_OPEN,
		"_Cancel", GTK_RESPONSE_CANCEL,
		"_Open", GTK_RESPONSE_ACCEPT, NULL);
	if (gtk_dialog_run(GTK_DIALOG(doopen))==GTK_RESPONSE_ACCEPT) {
		gchar *name = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(doopen));
		gapp->preq = (char*)name;
		gtkapp_load_data(gapp,name);
		g_free(name);
	}
	gtk_widget_destroy(doopen);
}
/*----------------------------------------------------------------------------*/
void gtkapp_todo_file_save(my1gtkapp_t* gapp) {
	char* pstr;
	gchar* name;
	int skip = 0;
	if (MY1DATA(gapp)->todo.task.size<=0||
			!tdlist_modified((my1todo_data_t*)&MY1DATA(gapp)->todo)) {
		fprintf(stderr,"@@ Nothing to save.\n");
		return;
	}
	pstr = gtkapp_get_filename(gapp);
	if (!pstr) {
		GtkWidget *dosave = gtk_file_chooser_dialog_new("Save ToDo.txt File",
			GTK_WINDOW(gapp->main),GTK_FILE_CHOOSER_ACTION_SAVE,
			"_Cancel", GTK_RESPONSE_CANCEL,
			"_Save", GTK_RESPONSE_ACCEPT, NULL);
		gtk_file_chooser_set_do_overwrite_confirmation(
			GTK_FILE_CHOOSER(dosave),TRUE);
		if (gtk_dialog_run(GTK_DIALOG(dosave))==GTK_RESPONSE_ACCEPT) {
			name = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dosave));
			cstr_assign(&gapp->nbuf,name);
			g_free(name);
		}
		else skip = 1;
		gtk_widget_destroy(dosave);
	}
	else cstr_assign(&gapp->nbuf,pstr);
	if (!skip) gtkapp_save_data(gapp,gapp->nbuf.buff);
}
/*----------------------------------------------------------------------------*/
#define QASK_NO 0
#define QASK_YES 1
#define QASK_CANCEL 2
/*----------------------------------------------------------------------------*/
void gtkapp_done_data(my1gtkapp_t* gapp) {
	int quit = 1;
	if (tdlist_modified((my1todo_data_t*)&MY1DATA(gapp)->todo)) {
		GtkWidget* qask = gtk_message_dialog_new((GtkWindow*)gapp->main,
			GTK_DIALOG_MODAL,GTK_MESSAGE_QUESTION,GTK_BUTTONS_NONE,
			"List changed! Save?");
		gtk_dialog_add_button(GTK_DIALOG(qask),"Yes",QASK_YES);
		gtk_dialog_add_button(GTK_DIALOG(qask),"No",QASK_NO);
		gtk_dialog_add_button(GTK_DIALOG(qask),"Cancel",QASK_CANCEL);
		switch (gtk_dialog_run(GTK_DIALOG(qask))) {
			case QASK_YES: gtkapp_todo_file_save(gapp); break;
			case QASK_CANCEL: quit = 0; break;
		}
		gtk_widget_destroy(qask);
	}
	if (quit) gtk_main_quit();
}
/*----------------------------------------------------------------------------*/
gboolean on_button_test(GtkWidget *widget, GdkEventButton *event,
		gpointer data) {
	int step = 0;
	gboolean done = FALSE;
	if (event->type == GDK_DOUBLE_BUTTON_PRESS) step = 2;
	else if (event->type == GDK_BUTTON_PRESS) step = 1;
	if (step) {
		fprintf(stderr,"@@ %s",step==2?"Double ":"");
		if (event->button == 1) fprintf(stderr,"Left");
		else if (event->button == 2) fprintf(stderr,"Middle");
		else if (event->button == 3) fprintf(stderr,"Right");
		else fprintf(stderr,"???");
		fprintf(stderr,"-Clicked!\n");
		done = TRUE;
	}
	return done;
}
/*----------------------------------------------------------------------------*/
/* these are defined in glade! */
void on_window_main_destroy(void) {
	gtk_main_quit();
}
/*----------------------------------------------------------------------------*/
void on_toolbutton_filter_toggled(GtkWidget *widget, gpointer data) {
	GtkWidget* pbox = (GtkWidget*) data;
	if (gtk_toggle_tool_button_get_active((GtkToggleToolButton*)widget))
		gtk_widget_show(pbox);
	else gtk_widget_hide(pbox);
}
/*----------------------------------------------------------------------------*/
