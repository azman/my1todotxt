/*----------------------------------------------------------------------------*/
#include "gtkapp.h"
#include "my1strtok.h"
#include "window_main.h"
#include "todo_debug.h"
#include "my1cstr_util.h"
/*----------------------------------------------------------------------------*/
gboolean on_combobox_context_clicked(GtkWidget *widget,
		GdkEventButton *event, gpointer data) {
	gboolean done = FALSE;
	my1gtkapp_t* gapp = (my1gtkapp_t*) data;
	if (event->type == GDK_DOUBLE_BUTTON_PRESS) {
		if (event->button == 1) /* L/M/R => 1/2/3*/ {
			GtkWidget *temp = gtk_bin_get_child((GtkBin*)gapp->cncb);
			if (temp) {
				gtk_entry_set_text((GtkEntry*)temp,"");
				gtkapp_pass_show(gapp);
				done = TRUE;
			}
		}
	}
	return done;
}
/*----------------------------------------------------------------------------*/
gboolean on_combobox_project_clicked(GtkWidget *widget,
		GdkEventButton *event, gpointer data) {
	gboolean done = FALSE;
	my1gtkapp_t* gapp = (my1gtkapp_t*) data;
	if (event->type == GDK_DOUBLE_BUTTON_PRESS) {
		if (event->button == 1) /* L/M/R => 1/2/3*/ {
			GtkWidget *temp = gtk_bin_get_child((GtkBin*)gapp->pjcb);
			if (temp) {
				gtk_entry_set_text((GtkEntry*)temp,"");
				gtkapp_pass_show(gapp);
				done = TRUE;
			}
		}
	}
	return done;
}
/*----------------------------------------------------------------------------*/
void on_button_todo_enter_clicked(GtkWidget *widget, gpointer data) {
	my1gtkapp_t* gapp = (my1gtkapp_t*) data;
	cstr_assign(&MY1DATA(gapp)->buff,(char*)
		gtk_entry_get_text((GtkEntry*)gapp->texe));
	cstr_trimws(&MY1DATA(gapp)->buff,1);
	if (MY1DATA(gapp)->buff.buff[0]=='/') {
		/* assumed to be a command */
		char *look;
		my1strtok_t find;
		strtok_init(&find," ");
		do {
			strtok_prep(&find,&MY1DATA(gapp)->buff);
			if (!strtok_next(&find)) {
				fprintf(stderr,"@@ Cannot find command???\n");
				break;
			}
			look = find.token.buff;
			if (!strncmp(look,"/open",6)) gtkapp_todo_file_open(gapp);
			else if (!strncmp(look,"/save",6)) gtkapp_todo_file_save(gapp);
			else if (!strncmp(look,"/full",5)) {
				if (!(MY1DATA(gapp)->flag&FLAG_DO_FULL)) {
					MY1DATA(gapp)->flag |= FLAG_DO_FULL;
					gtkapp_data_update(gapp);
				}
			}
			else if (!strncmp(look,"/norm",5)) {
				if (MY1DATA(gapp)->flag&FLAG_DO_FULL) {
					MY1DATA(gapp)->flag &= ~FLAG_DO_FULL;
					gtkapp_data_update(gapp);
				}
			}
			else if (!strncmp(look,"/uuid",5)) {
				if (!(MY1DATA(gapp)->flag&FLAG_CHKUUID)) {
					MY1DATA(gapp)->flag |= FLAG_CHKUUID;
					gtkapp_data_update(gapp);
				}
			}
			else if (!strncmp(look,"/noid",5)) {
				if (MY1DATA(gapp)->flag&FLAG_CHKUUID) {
					MY1DATA(gapp)->flag &= ~FLAG_CHKUUID;
					gtkapp_data_update(gapp);
				}
			}
			else fprintf(stderr,"@@ Unknown command '%s'!\n",look);
		} while (0);
		strtok_free(&find);
	}
	else if (gapp->pick) /* edit */ {
		gapp->task = (my1todo_task_t*)
			g_object_get_data(G_OBJECT(gapp->pick),MY1TASK_KEY);
		if (strncmp(MY1DATA(gapp)->buff.buff,gapp->tbuf.buff,gapp->tbuf.fill)) {
			cstr_addcol(&MY1DATA(gapp)->buff,
				gapp->ibuf.buff,MY1CSTR_PREFIX_SPACING);
			tdtask_prep_str(gapp->task,&gapp->ibuf);
			cstr_addcol(&gapp->ibuf,MY1DATA(gapp)->buff.buff,
				MY1CSTR_PREFIX_SPACING);
			tdtask_read(gapp->task,gapp->ibuf.buff);
			tdtask_prep(gapp->task);
			cstr_assign(&gapp->task->text,gapp->task->prep.buff);
			gtkapp_data_update(gapp);
			gtkapp_todo_modified(gapp,1);
		}
		else fprintf(stderr,"@@ No changes!\n\t>>{%s}\n\t<<[%s]\n",
			MY1DATA(gapp)->buff.buff,gapp->tbuf.buff);
	}
	else /* new entry */ {
		if (MY1DATA(gapp)->buff.fill>0) {
			/* add context/project if picked */
			gchar *ppro, *pcon;
			ppro = gtk_combo_box_text_get_active_text(
				(GtkComboBoxText*)gapp->pjcb);
			if (ppro&&ppro[0]=='+') {
				if (cstr_findwd(&MY1DATA(gapp)->buff,(char*)ppro)<0) {
					cstr_addcol(&MY1DATA(gapp)->buff,(char*)ppro,
						MY1CSTR_PREFIX_SPACING);
				}
			}
			pcon = gtk_combo_box_text_get_active_text(
				(GtkComboBoxText*)gapp->cncb);
			if (pcon&&pcon[0]=='@') {
				if (cstr_findwd(&MY1DATA(gapp)->buff,(char*)pcon)<0) {
					cstr_addcol(&MY1DATA(gapp)->buff,(char*)pcon,
						MY1CSTR_PREFIX_SPACING);
				}
			}
			gapp->task = tdlist_make_task(&MY1DATA(gapp)->todo,
				MY1DATA(gapp)->buff.buff);
			if (!gapp->task)
				fprintf(stderr,"** Cannot make task!(%d)\n",
					MY1DATA(gapp)->todo.code);
			else {
				tdtask_prep(gapp->task);
				cstr_assign(&gapp->task->text,gapp->task->prep.buff);
				gtkapp_data_update(gapp);
				gtkapp_todo_modified(gapp,1);
			}
		}
		/*else fprintf(stderr,"@@ Empty string!\n");*/
	}
	gtk_entry_set_text((GtkEntry*)gapp->texe,"");
	gapp->pick = 0x0;
	gtk_widget_set_sensitive(gapp->hbar,TRUE);
	gtk_widget_set_sensitive(gapp->bfil,TRUE);
	gtk_widget_set_sensitive(gapp->swin,TRUE);
}
/*----------------------------------------------------------------------------*/
void on_button_todo_reset_clicked(GtkWidget *widget, gpointer data) {
	my1gtkapp_t* gapp = (my1gtkapp_t*) data;
	gtk_entry_set_text((GtkEntry*)gapp->texe,"");
	gapp->pick = 0x0;
	gtk_widget_set_sensitive(gapp->hbar,TRUE);
	gtk_widget_set_sensitive(gapp->bfil,TRUE);
	gtk_widget_set_sensitive(gapp->swin,TRUE);
}
/*----------------------------------------------------------------------------*/
gboolean on_key_press(GtkWidget *widget, GdkEventKey *kevent, gpointer data) {
	my1gtkapp_t* gapp = (my1gtkapp_t*) data;
	if (kevent->type == GDK_KEY_PRESS) {
		if (widget==gapp->texe) {
			if (kevent->keyval==GDK_KEY_Return) {
				on_button_todo_enter_clicked(widget,data);
				return TRUE;
			}
		}
		else {
			/**g_message("@@ DEBUG:{%d,%c}",kevent->keyval,kevent->keyval);*/
			if (kevent->keyval==GDK_KEY_Escape||kevent->keyval==GDK_KEY_q) {
				gtkapp_done_data(gapp);
				return TRUE;
			}
		}
	}
	return FALSE;
}
/*----------------------------------------------------------------------------*/
void gtkapp_build_gui(my1gtkapp_t* gapp, char* glade_xml) {
	GtkBuilder *builder;
	/* build interface (glade) */
	builder = gtk_builder_new();
	gtk_builder_add_from_string(builder,glade_xml,-1,0x0);
	/* get widget refs */
	gapp->main = GTK_WIDGET(gtk_builder_get_object(builder,"window_main"));
	gapp->hbar = GTK_WIDGET(gtk_builder_get_object(builder,"header_main"));
	gapp->swin = GTK_WIDGET(gtk_builder_get_object(builder,"scrollwin_task"));
	gapp->bfil = GTK_WIDGET(gtk_builder_get_object(builder,"box_filter"));
	gapp->view = GTK_WIDGET(gtk_builder_get_object(builder,"viewport_task"));
	gapp->vbox = 0x0; /* dynamically created gtkbox - placed in view */
	gapp->open = GTK_WIDGET(gtk_builder_get_object(builder,"toolbutton_open"));
	gapp->save = GTK_WIDGET(gtk_builder_get_object(builder,"toolbutton_save"));
	gapp->sort = GTK_WIDGET(gtk_builder_get_object(builder,"toolbutton_sort"));
	gapp->tfil = GTK_WIDGET(gtk_builder_get_object(builder,
		"toolbutton_filter"));
	gapp->cncb = GTK_WIDGET(gtk_builder_get_object(builder,"combobox_context"));
	gapp->pjcb = GTK_WIDGET(gtk_builder_get_object(builder,"combobox_project"));
	gapp->texe = GTK_WIDGET(gtk_builder_get_object(builder,"entry_todo"));
	gapp->bexe = GTK_WIDGET(gtk_builder_get_object(builder,
		"button_todo_enter"));
	gapp->rexe = GTK_WIDGET(gtk_builder_get_object(builder,
		"button_todo_reset"));
	/* dynamic popup menu */
	gapp->menu = gtk_menu_new();
	gtk_widget_show(gapp->menu);
	/* connect signals defined in glade - make sure functions defined */
	gtk_builder_connect_signals(builder,0x0);
	/* free build resources */
	g_object_unref(builder);
	g_signal_connect(G_OBJECT(gapp->main),"key_press_event",
		G_CALLBACK(on_key_press),(gpointer)gapp);
	g_signal_connect(G_OBJECT(gapp->texe),"key_press_event",
		G_CALLBACK(on_key_press),(gpointer)gapp);
	g_signal_connect(G_OBJECT(gapp->cncb),"button-press-event",
		G_CALLBACK(on_combobox_context_clicked),(gpointer)gapp);
	g_signal_connect(G_OBJECT(gapp->pjcb),"button-press-event",
		G_CALLBACK(on_combobox_project_clicked),(gpointer)gapp);
	g_signal_connect(G_OBJECT(gapp->bexe),"clicked",
		G_CALLBACK(on_button_todo_enter_clicked),(gpointer)gapp);
	g_signal_connect(G_OBJECT(gapp->rexe),"clicked",
		G_CALLBACK(on_button_todo_reset_clicked),(gpointer)gapp);
	/* swapped => ignore widget pointers! */
	g_signal_connect_swapped(G_OBJECT(gapp->main),"delete-event",
		G_CALLBACK(gtkapp_done_data),(gpointer)gapp);
	g_signal_connect_swapped(G_OBJECT(gapp->open),"clicked",
		G_CALLBACK(gtkapp_todo_file_open),(gpointer)gapp);
	g_signal_connect_swapped(G_OBJECT(gapp->save),"clicked",
		G_CALLBACK(gtkapp_todo_file_save),(gpointer)gapp);
	g_signal_connect_swapped(G_OBJECT(gapp->sort),"clicked",
		G_CALLBACK(gtkapp_todo_file_sort),(gpointer)gapp);
	g_signal_connect_swapped(G_OBJECT(gapp->cncb),"changed",
		G_CALLBACK(gtkapp_pass_show),(gpointer)gapp);
	g_signal_connect_swapped(G_OBJECT(gapp->pjcb),"changed",
		G_CALLBACK(gtkapp_pass_show),(gpointer)gapp);
	/* show/start main interface */
	gtk_widget_show(gapp->main);
	/* prepare stuff here */
	if (gapp->psrc) {
		gapp->temp = data_path(MY1DATA(gapp),gapp->psrc);
		if (!gapp->temp)
			gtkapp_load_data(gapp,gapp->psrc);
		else if (gapp->preq)
			fprintf(stderr,"** Missing file '%s'!\n",gapp->psrc);
		gapp->psrc = 0x0;
	}
}
/*----------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
	my1gtkapp_t gtkapp;
	gtk_init(&argc,&argv);
	gtkapp_init_data(&gtkapp,argc,argv);
	gtkapp_build_gui(&gtkapp,window_main);
	gtk_main();
	gtkapp_free_data(&gtkapp);
	return 0;
}
/*----------------------------------------------------------------------------*/
