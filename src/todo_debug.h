/*----------------------------------------------------------------------------*/
#ifndef __MY1TODO_DEBUG_H__
#define __MY1TODO_DEBUG_H__
/*----------------------------------------------------------------------------*/
#include "todo_task.h"
/*----------------------------------------------------------------------------*/
void todo_show_task(my1todo_task_t* task) {
	my1todo_name_t* name;
	my1todo_tags_t* tags;
	int temp;
	fprintf(stderr,"## Task: %s\n",task->task.buff);
	temp = tddata_flag_get((my1todo_data_t*)task,TDTASK_FLAG_COMPLETED);
	if (task->prio.fill>0) {
		fprintf(stderr,"   -");
		fprintf(stderr," Priority: %s\n",task->prio.buff);
	}
	if (task->made.date!=DATE_INVALID) {
		fprintf(stderr,"   -");
		if (task->done.date!=DATE_INVALID) {
			fprintf(stderr," Completed: %s",date_2str(&task->done,0x0));
			if (!temp) fprintf(stderr," [NOFLAG]");
		}
		else
			if (temp) fprintf(stderr," [NODATE:Completion]");
		fprintf(stderr," Created: %s\n",date_2str((my1date_t*)&task->made,0x0));
	}
	if (task->cons.size>0) {
		fprintf(stderr,"   - Context:");
		list_scan_prep(&task->cons);
		while (list_scan_item(&task->cons)) {
			name = (my1todo_name_t*) task->cons.curr->data;
			fprintf(stderr," %s",name->name.buff);
		}
		fprintf(stderr,"\n");
	}
	if (task->pros.size>0) {
		fprintf(stderr,"   - Project:");
		list_scan_prep(&task->pros);
		while (list_scan_item(&task->pros)) {
			name = (my1todo_name_t*) task->pros.curr->data;
			fprintf(stderr," %s",name->name.buff);
		}
		fprintf(stderr,"\n");
	}
	if (task->tags.size>0) {
		fprintf(stderr,"   - [TAGS]");
		list_scan_prep(&task->tags);
		while (list_scan_item(&task->tags)) {
			tags = (my1todo_tags_t*) task->tags.curr->data;
			fprintf(stderr," %s:%s",tags->akey.buff,tags->aval.buff);
		}
		fprintf(stderr,"\n");
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1TODO_DEBUG_H__ */
/*----------------------------------------------------------------------------*/
