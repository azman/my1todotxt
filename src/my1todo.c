/*----------------------------------------------------------------------------*/
#include "data.h"
#include "my1cstr_util.h"
#include <string.h>
/*----------------------------------------------------------------------------*/
#define STAT_ERROR_MISS (STAT_ERROR|(STAT_EFLAG_LAST<<1))
#define STAT_ERROR_MAKE (STAT_ERROR|(STAT_EFLAG_LAST<<2))
#define STAT_ERROR_PREP (STAT_ERROR|(STAT_EFLAG_LAST<<3))
#define STAT_ERROR_TASK (STAT_ERROR|(STAT_EFLAG_LAST<<4))
/*----------------------------------------------------------------------------*/
#define FLAG_DO_MAKE (FLAG_LFLAG<<1)
#define FLAG_DO_SAVE (FLAG_LFLAG<<2)
#define FLAG_DO_LIST (FLAG_LFLAG<<3)
#define FLAG_DO_FULL (FLAG_LFLAG<<4)
#define FLAG_DO_TASK (FLAG_LFLAG<<5)
#define FLAG_DODEBUG (FLAG_LFLAG<<6)
#define FLAG_CHKUUID (FLAG_LFLAG<<7)
#define FLAG_CHKDONE (FLAG_LFLAG<<8)
#define FLAG_VERBOSE (FLAG_LFLAG<<9)
/*----------------------------------------------------------------------------*/
#define dptr(data) ((data_t*)data)
#define data_loud(data,mesg) if (data->flag&FLAG_VERBOSE) printf(mesg)
#define data_talk(data,mesg) data_loud(dptr(data),mesg)
#define data_done(data) !(dptr(data)->flag&FLAG_DO_SAVE)
/*----------------------------------------------------------------------------*/
void data_args(data_t* data, int argc, char* argv[]) {
	data->stat = STAT_OK; data->flag = 0;
	data->psrc = DEFAULT_TODOTXT;
	data->pcon = 0x0; data->ppro = 0x0;
	for (data->loop=1;data->loop<argc;data->loop++) {
		/* first non-option is the start of new task! */
		if (argv[data->loop][0]!='-') break;
		if (!strncmp(argv[data->loop],"--file",7)) {
			if (++data->loop==argc) break;
			data->psrc = argv[data->loop];
		}
		else if (!strncmp(argv[data->loop],"--pick",7)) {
			if (++data->loop==argc) break;
			switch (argv[data->loop][0]) {
				case '@': data->pcon = argv[data->loop]; break;
				case '+': data->ppro = argv[data->loop]; break;
				default:
					fprintf(stderr,"** Invalid pick '%s'!\n",argv[data->loop]);
			}
		}
		else if (!strncmp(argv[data->loop],"--done",7)) {
			if (++data->loop==argc) break;
			date_time_my1str((my1date_time_t*)&data->uuid,argv[data->loop]);
			if (!time_check(&data->uuid))
				data->flag |= FLAG_CHKDONE;
			else fprintf(stderr,"** Invalid uuid '%s'!\n",argv[data->loop]);
		}
		else if (!strncmp(argv[data->loop],"--list",7))
			data->flag |= FLAG_DO_LIST; /* flag to show all context/project */
		else if (!strncmp(argv[data->loop],"--make",7))
			data->flag |= FLAG_DO_MAKE; /* flag to create new todo file */
		else if (!strncmp(argv[data->loop],"--save",7))
			data->flag |= FLAG_DO_SAVE; /* flag to save/update todo file */
		else if (!strncmp(argv[data->loop],"--full",7))
			data->flag |= FLAG_DO_FULL; /* flag to show full task line */
		else if (!strncmp(argv[data->loop],"--uuid",7))
			data->flag |= FLAG_CHKUUID; /* flag to show task's uuid */
		else if (!strncmp(argv[data->loop],"--due",6))
			data->flag |= FLAG_LISTDUE; /* flag to show due projects */
		else if (!strncmp(argv[data->loop],"--my1form",10))
			data->flag |= FLAG_MY1FORM; /* flag to use my1form */
		else if (!strncmp(argv[data->loop],"--debug",8))
			data->flag |= FLAG_DODEBUG;
		else if (!strncmp(argv[data->loop],"--verbose",10))
			data->flag |= FLAG_VERBOSE;
		else fprintf(stderr,"** Unknown option '%s'!\n",argv[data->loop]);
	}
}
/*----------------------------------------------------------------------------*/
int data_prep(data_t* data) {
	do {
		/* are we adding new? */
		if (data->buff.fill>0) {
			if (data->flag&FLAG_VERBOSE)
				printf("-- Adding new:{%s}\n",data->buff.buff);
			data->task = tdlist_make_task(&data->todo,data->buff.buff);
			if (!data->task) {
				data->stat |= STAT_ERROR_MAKE;
				fprintf(stderr,"** Cannot make task!(%d:%d)\n",
					data->temp,data->todo.code);
			}
			else {
				data->flag |= FLAG_DO_TASK;
				fprintf(stderr,"@@ NEWTASK:{%s}\n",data->task->text.buff);
			}
		}
		/* marking task as done if requested */
		if (data->flag&FLAG_CHKDONE) {
			data->task = tdlist_find_task(&data->todo,&data->uuid);
			if (!data->task) {
				data->stat |= STAT_ERROR_TASK;
				fprintf(stderr,"@@ Cannot find uuid [%08d%06d]!\n",
					data->uuid.date,data->uuid.time);
			}
			else {
				cstr_setstr(&data->buff,data->task->text.buff);
				data->temp = tdtask_done(data->task);
				if (data->temp<0) {
					data->stat |= STAT_ERROR_TASK;
					fprintf(stderr,"** Failed to mark [%08d%06d] complete!\n",
						data->uuid.date,data->uuid.time);
				}
				else {
					data->flag |= FLAG_DO_TASK;
					fprintf(stderr,"@@ OLDTASK:{%s}\n",data->buff.buff);
					fprintf(stderr,"@@ ENDTASK:{%s}\n",data->task->text.buff);
				}
			}
		}
		if (data->flag&FLAG_DO_LIST) {
			my1todo_name_t* name;
			my1list_t* list;
			list = &data->todo.cons;
			if (list->size>0) {
				fprintf(stderr,"@@ [CONS]");
				list_scan_prep(list);
				while (list_scan_item(list)) {
					name = (my1todo_name_t*) list->curr->data;
					fprintf(stderr," %s",name->name.buff);
				}
				fprintf(stderr,"\n");
			}
			list = &data->todo.pros;
			if (list->size>0) {
				fprintf(stderr,"@@ [PROS]");
				list_scan_prep(list);
				while (list_scan_item(list)) {
					name = (my1todo_name_t*) list->curr->data;
					fprintf(stderr," %s",name->name.buff);
				}
				fprintf(stderr,"\n");
			}
		}
	} while (0);
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_task(data_t* data) {
	if (data->flag&FLAG_MY1FORM) {
		/* prepare my1todotxt format */
		data_loud(data,"-- Reformatting tasks\n");
		tdlist_prep(&data->todo);
		if (!tdlist_sortdiff(&data->todo)) {
			data_loud(data,"-- List Verified!\n");
		}
		else {
			if (!(data->flag&FLAG_DO_TASK))
				fprintf(stderr,"@@ Modified to my1todotxt format!\n");
		}
		/* make sure using newly prepped */
		data_loud(data,"-- Reorganizing tasks\n");
		tdlist_prep2text(&data->todo);
	}
	/* always sort */
	data_loud(data,"-- Sorting tasks\n");
	tdlist_sort(&data->todo);
	if (tdlist_sortdiff(&data->todo)) {
		if (!(data->flag&FLAG_DO_TASK))
			fprintf(stderr,"@@ List is NOT sorted!\n");
	}
	/* if not doing anything, view data */
	if (!(data->flag&FLAG_DO_TASK)||(data->flag&FLAG_DO_LIST)) {
		data_loud(data,"-- Filtering tasks\n");
		data_pass(data,data->pcon,data->ppro);
		data_loud(data,"-- Data VIEW\n");
		data->temp = 0;
		if (data->flag&FLAG_CHKUUID) data->temp |= TDTASK_VIEW_UUID;
		if (data->flag&FLAG_DO_FULL) data->temp |= TDTASK_VIEW_FULL;
		list_scan_prep(data->show);
		while (list_scan_item(data->show)) {
			data->task = (my1todo_task_t*) data->show->curr->data;
			tdtask_view_str(data->task,&data->buff,data->temp);
			if (data->flag&FLAG_DO_FULL) {
				fprintf(stdout,"%s\n",data->buff.buff);
				continue;
			}
			/* skip if task completed */
			if (tdtask_completed(data->task)) continue;
			fprintf(stdout,"%s\n",data->buff.buff);
		}
	}
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int data_make(data_t* data, int argc, char* argv[]) {
	/* new task if we have more here */
	for (;data->loop<argc;data->loop++)
		cstr_addcol(&data->buff,argv[data->loop],MY1CSTR_PREFIX_SPACING);
	/* try to load todo file */
	if (data_path(data,data->psrc)) {
		if (!(data->flag&FLAG_DO_MAKE)) {
			fprintf(stderr,"** Missing file '%s'!\n",data->psrc);
			data->stat |= STAT_ERROR_MISS;
		}
	}
	else {
		data_loud(data,"-- Data LOAD\n");
		data_load(data,data->name.full);
	}
	return data->stat;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	data_t data;
	data_args(&data,argc,argv);
	data_talk(&data,"-- Data INIT\n");
	data_init(&data);
	do {
		if (data_make(&data,argc,argv)) break;
		if (data_prep(&data)) break;
		if (data_task(&data)) break;
		if (data_done(&data)) break;
		data_talk(&data,"-- Data SAVE\n");
		if (data_save(&data,data.psrc)) break;
	} while (0);
	data_talk(&data,"-- Data FREE\n");
	data_free(&data);
	return data.stat;
}
/*----------------------------------------------------------------------------*/
