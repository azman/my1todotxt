/*----------------------------------------------------------------------------*/
#ifndef __MY1TODO_TASK_H__
#define __MY1TODO_TASK_H__
/*----------------------------------------------------------------------------*/
#include "todo_data.h"
/*----------------------------------------------------------------------------*/
/* flag for task */
#define TDTASK_FLAG_COMPLETED 0x01
/*----------------------------------------------------------------------------*/
typedef struct _my1todo_task_t {
	my1date_time_t uuid; /* creation timestamp (from uid: tag) */
	my1time_t made; /* creation date + time (should == uuid.date/time)*/
	my1date_t done; /* completion date */
	my1date_t ddue; /* due date (from due: tag) */
	my1cstr_t text; /* line entry in file */
	my1cstr_t prep; /* buff entry created from current data */
	my1cstr_t prio; /* priority (becomes pri: tag when task_done) */
	my1cstr_t task; /* main task (includes context, project, tags) */
	my1list_t cons; /* list of my1todo_name_t* (context) */
	my1list_t pros; /* list of my1todo_name_t* (project) */
	my1list_t tags; /* list of my1todo_tags_t (key:value) */
	my1todo_type_t* type; /* top level list */
	void* vobj; /* general-purpose pointer (e.g. view object?) */
} my1todo_task_t;
/*----------------------------------------------------------------------------*/
#define TDTASK_ERROR 0x8000
#define TDTASK_ERROR_EMPTY_LINE 0x0001
#define TDTASK_ERROR_INVALID1_LINE 0x0002
#define TDTASK_ERROR_INVALID2_LINE 0x0004
#define TDTASK_ERROR_INVALID3_LINE 0x0008
#define TDTASK_ERROR_INVALID4_LINE 0x0010
#define TDTASK_ERROR_INVALID5_LINE 0x0020
#define TDTASK_ERROR_INVALID6_LINE 0x0040
#define TDTASK_ERROR_INVALID7_LINE 0x0080
#define TDTASK_ERROR_REMOVING_DDUE (TDTASK_ERROR|0x0200)
#define TDTASK_ERROR_REMOVING_TDUE (TDTASK_ERROR|0x0400)
#define TDTASK_ERROR_REMOVING_PRIO (TDTASK_ERROR|0x0800)
/*----------------------------------------------------------------------------*/
#define TDTASK_DUE_NONE 0
/* actually, any negative value */
#define TDTASK_DUE_PAST -1
/* viewing options */
#define TDTASK_VIEW_UUID 0x00001
#define TDTASK_VIEW_FULL 0x00002
#define TDTASK_VIEW_DEFAULT (TDTASK_VIEW_UUID|TDTASK_VIEW_FULL)
/*----------------------------------------------------------------------------*/
void tdtask_init(my1todo_task_t* task, my1todo_type_t* type);
void tdtask_free(my1todo_task_t* task);
int tdtask_read(my1todo_task_t* task, char* line);
int tdtask_prep(my1todo_task_t* task); /* prepare todotxt entry from data */
int tdtask_prep_str(my1todo_task_t* task, my1cstr_t* pfix);
int tdtask_view_str(my1todo_task_t* task, my1cstr_t* buff, int mask);
int tdtask_calc_due(my1todo_task_t* task, my1date_t* curr);
int tdtask_form_my1(my1todo_task_t* task);
int tdtask_done(my1todo_task_t* task); /* add completion date & remove due */
my1todo_tags_t* tdtask_find_tags(my1todo_task_t* task, char* akey);
my1todo_tags_t* tdtask_make_tags(my1todo_task_t* task, my1todo_tags_t* from);
/*----------------------------------------------------------------------------*/
/** useful macro */
#define tdtask_find_cons(task,name) tdtype_find(&task->cons,name)
#define tdtask_find_pros(task,name) tdtype_find(&task->pros,name)
#define tdtask_complete(pt) tddata_flag_set(TDDATA(pt),TDTASK_FLAG_COMPLETED)
#define tdtask_incomplete(pt) tddata_flag_clr(TDDATA(pt),TDTASK_FLAG_COMPLETED)
#define tdtask_completed(pt) tddata_flag_get(TDDATA(pt),TDTASK_FLAG_COMPLETED)
/*----------------------------------------------------------------------------*/
#endif /** __MY1TODO_TASK_H__ */
/*----------------------------------------------------------------------------*/
